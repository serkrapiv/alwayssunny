//
//  BaseRequestManagerServiceTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "BaseRequestManagerServiceTests.h"

#import "RequestManager.h"
#import "OCMock.h"

@implementation BaseRequestManagerServiceTests

- (id<RequestManager>)requestManagerMockWithResult:(id)result
                                             error:(NSError *)error {
    
    id requestManagerMock = OCMStrictProtocolMock(@protocol(RequestManager));
    OCMStub([requestManagerMock performRequestWithMethod:[OCMArg any]
                                                urlParts:[OCMArg any]
                                            requestModel:[OCMArg any]
                                          mappingContext:[OCMArg any]
                                              completion:[OCMArg any]])
    .andDo(^(NSInvocation *invocation)
           {
               [invocation retainArguments];
               ServiceCompletionBlockWithArrayOfObjects completionBlock;
               [invocation getArgument:&completionBlock atIndex:6];
               [completionBlock copy];
               NSArray *objects = result ? result : nil;
               completionBlock(objects, error);
           });
    return requestManagerMock;
}

- (NSError *)errorForTestPurposes {
    
    NSError *error = [[NSError alloc] initWithDomain:@"RCCTestDomain" code:1337 userInfo:nil];
    
    return error;
}

@end