//
//  BaseRequestManagerServiceTests.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <XCTest/XCTest.h>

@protocol RequestManager;

/**
 @author Sergey Krapivenskiy, 16-03-17 20:03:29
 
 Base class for services that use RequestManager
 */
@interface BaseRequestManagerServiceTests : XCTestCase

- (id<RequestManager>)requestManagerMockWithResult:(id)result
                                             error:(NSError *)error;

- (NSError *)errorForTestPurposes;

@end