//
//  ForecastServiceBaseTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "BaseRequestManagerServiceTests.h"

#import "OCMock.h"
#import "OHHTTPStubs.h"

#import "ForecastServiceBase.h"

#import "DailyForecast.h"

#import "TestConstants.h"

@interface ForecastServiceBaseTests : BaseRequestManagerServiceTests

@property (nonatomic, strong) ForecastServiceBase *service;

@end

@implementation ForecastServiceBaseTests

#pragma mark - Lifecycle

- (void)setUp {
    [super setUp];
    self.service = [ForecastServiceBase new];
}

- (void)tearDown {
    self.service = nil;
    [super tearDown];
}

#pragma mark - Tests

- (void)testThatForecastsAreObtained {
    //given
    XCTestExpectation *expectation = [self expectationWithDescription:@"forecast success"];
    
    NSArray *forecasts = [self forecastsForTest];
    
    id<RequestManager> requestManager = [self requestManagerMockWithResult:forecasts
                                                                     error:nil];
    
    self.service.requestManager = requestManager;
    
    __block NSArray *receivedObjects = nil;
    __block NSError *receivedError = nil;
    
    //when
    [self.service loadForecastsForCityWithName:@"Dublin"
                                  numberOfDays:5
                                    completion:^(NSArray *objects, NSError *error)
     {
         receivedObjects = objects;
         receivedError = error;
         dispatch_async(dispatch_get_main_queue(), ^{
             [expectation fulfill];
         });
    }];
    
    //then
    [self waitForExpectationsWithTimeout:ALSDefaultTestTimeout
                                 handler:^(NSError * _Nullable error)
     {
         XCTAssertNil(receivedError);
         XCTAssertEqualObjects(forecasts, receivedObjects);
     }];
}

- (void)testThatServiceReturnsErrorWhenFailed {
    
    //given
    XCTestExpectation *expectation = [self expectationWithDescription:@"forecasts fail"];
    NSError *expectedError = [self errorForTestPurposes];
    
    id<RequestManager> requestManager = [self requestManagerMockWithResult:nil
                                                                     error:expectedError];
    self.service.requestManager = requestManager;
    
    __block NSArray *receivedObjects = nil;
    __block NSError *receivedError = nil;
    
    //when
    [self.service loadForecastsForCityWithName:@"Dublin"
                                  numberOfDays:5
                                    completion:^(NSArray *objects, NSError *error)
     {
         receivedObjects = objects;
         receivedError = error;
         dispatch_async(dispatch_get_main_queue(), ^{
             [expectation fulfill];
         });
     }];
    
    //then
    [self waitForExpectationsWithTimeout:ALSDefaultTestTimeout
                                 handler:^(NSError * _Nullable error)
     {
         XCTAssertEqualObjects(receivedError, expectedError);
         XCTAssertNil(receivedObjects);
     }];
}

#pragma mark - Helpers

- (NSArray *)forecastsForTest {
    
    DailyForecast *first = [DailyForecast new];
    DailyForecast *second = [DailyForecast new];
    
    NSArray *forecasts = @[ first, second ];
    
    return forecasts;
}

@end
