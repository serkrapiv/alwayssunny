//
//  CitiesServiceTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "BaseRequestManagerServiceTests.h"

#import "OCMock.h"
#import "OHHTTPStubs.h"

#import "CitiesServiceBase.h"

#import "City.h"

#import "TestConstants.h"

@interface CitiesServiceTests : BaseRequestManagerServiceTests

@property (nonatomic, strong) CitiesServiceBase *service;

@end

@implementation CitiesServiceTests

#pragma mark - Lifecycle

- (void)setUp {
    [super setUp];
    self.service = [CitiesServiceBase new];
}

- (void)tearDown {
    self.service = nil;
    [super tearDown];
}

#pragma mark - Tests

- (void)testThatCitiesAreLoadedFromDevice {
    // given
    NSArray *expectedCities = [self citiesForTest];
    
    id mockStandardDefaults = OCMClassMock([NSUserDefaults class]);
    self.service.userDefaults = mockStandardDefaults;
    
    OCMStub([mockStandardDefaults objectForKey:[OCMArg isEqual:ALSSavedCitiesKey]]).andReturn([NSKeyedArchiver archivedDataWithRootObject:expectedCities]);
    
    // when
    NSArray *receivedObjects = [self.service obtainSavedCities];
    
    // then
    XCTAssertEqualObjects(receivedObjects, expectedCities);
    
    // cleanup
    [mockStandardDefaults stopMocking];
    mockStandardDefaults = nil;
}

- (void)testThatCityIsAdded {
    // given
    NSArray *existingCities = [self citiesForTest];
    
    id mockStandardDefaults = OCMClassMock([NSUserDefaults class]);
    self.service.userDefaults = mockStandardDefaults;
    
    OCMStub([mockStandardDefaults objectForKey:[OCMArg isEqual:ALSSavedCitiesKey]]).andReturn([NSKeyedArchiver archivedDataWithRootObject:existingCities]);
    
    City *newCity = [City new];
    newCity.name = @"Moscow";
    newCity.longitude = 10.0;
    newCity.latitude = 15.3;
    
    NSArray *expectedCities = [existingCities arrayByAddingObjectsFromArray:@[ newCity] ];
    
    BOOL (^checkBlock)(id) = ^BOOL(NSData *savedObject) {
        NSArray *savedObjects = [NSKeyedUnarchiver unarchiveObjectWithData:savedObject];
        return [savedObjects isEqualToArray:expectedCities];
    };
    
    // when
    [self.service addCity:newCity];
    
    // then
    OCMVerify([mockStandardDefaults setObject:[OCMArg checkWithBlock:checkBlock] forKey:[OCMArg isEqual:ALSSavedCitiesKey]]);
    OCMVerify([mockStandardDefaults synchronize]);
    
    // cleanup
    [mockStandardDefaults stopMocking];
    mockStandardDefaults = nil;
}

- (void)testThatCityIsRemoved {
    // given
    City *dublin = [City new];
    dublin.name = @"Dublin";
    dublin.longitude = 15.0;
    dublin.latitude = 13.0;
    
    City *newYork = [City new];
    newYork.name = @"New York";
    newYork.latitude = 30.0;
    newYork.longitude = 45.6;
    
    NSArray *cities = @[ dublin, newYork ];
    
    id mockStandardDefaults = OCMClassMock([NSUserDefaults class]);
    self.service.userDefaults = mockStandardDefaults;
    
    OCMStub([mockStandardDefaults objectForKey:[OCMArg isEqual:ALSSavedCitiesKey]]).andReturn([NSKeyedArchiver archivedDataWithRootObject:cities]);
    
    NSArray *expectedCities = @[ dublin ];
    
    BOOL (^checkBlock)(id) = ^BOOL(NSData *savedObject) {
        NSArray *savedObjects = [NSKeyedUnarchiver unarchiveObjectWithData:savedObject];
        return [savedObjects isEqualToArray:expectedCities];
    };
    
    // when
    [self.service removeCity:newYork];
    
    // then
    OCMVerify([mockStandardDefaults setObject:[OCMArg checkWithBlock:checkBlock] forKey:[OCMArg isEqual:ALSSavedCitiesKey]]);
    OCMVerify([mockStandardDefaults synchronize]);
    
    // cleanup
    [mockStandardDefaults stopMocking];
    mockStandardDefaults = nil;
}

- (void)testThatCitiesForSearchStringAreObtained {
    //given
    XCTestExpectation *expectation = [self expectationWithDescription:@"cities success"];
    
    NSArray *cities = [self citiesForTest];
    
    id<RequestManager> requestManager = [self requestManagerMockWithResult:cities
                                                                     error:nil];
    
    self.service.requestManager = requestManager;
    NSString *searchString = @"Rome";
    
    __block NSArray *receivedObjects = nil;
    __block NSError *receivedError = nil;
    
    //when
    [self.service loadCitiesForSearchString:searchString
                               withCompletion:^(NSArray *objects, NSError *error)
     {
         receivedObjects = objects;
         receivedError = error;
         dispatch_async(dispatch_get_main_queue(), ^{
             [expectation fulfill];
         });
     }];
    
    //then
    [self waitForExpectationsWithTimeout:ALSDefaultTestTimeout
                                 handler:^(NSError * _Nullable error)
     {
         XCTAssertNil(receivedError);
         XCTAssertEqualObjects(cities, receivedObjects);
     }];
}

- (void)testThatCitiesServiceReturnsErrorWhenFailed {
    
    //given
    XCTestExpectation *expectation = [self expectationWithDescription:@"cities fail"];
    NSError *expectedError = [self errorForTestPurposes];
    
    id<RequestManager> requestManager = [self requestManagerMockWithResult:nil
                                                                     error:expectedError];
    self.service.requestManager = requestManager;
    NSString *searchString = @"Rome";
    
    __block NSArray *receivedObjects = nil;
    __block NSError *receivedError = nil;
    
    //when
    [self.service loadCitiesForSearchString:searchString
                             withCompletion:^(NSArray *objects, NSError *error)
     {
         receivedObjects = objects;
         receivedError = error;
         dispatch_async(dispatch_get_main_queue(), ^{
             [expectation fulfill];
         });
     }];
    
    //then
    [self waitForExpectationsWithTimeout:ALSDefaultTestTimeout
                                 handler:^(NSError * _Nullable error)
     {
         XCTAssertEqualObjects(receivedError, expectedError);
         XCTAssertNil(receivedObjects);
     }];
}

#pragma mark - Helpers

- (NSArray *)citiesForTest {
    City *first = [City new];
    first.name = @"Dublin";
    first.latitude = 40.0;
    first.longitude = 20.0;
    
    City *second = [City new];
    second.name = @"New York";
    second.latitude = 50.0;
    second.longitude = 30.0;
    
    NSArray *cities = @[ first, second ];
    
    return cities;
}

@end
