//
//  NSDictionary+ALSJSONFromFile.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (ALSJSONFromFile)

/**
 @author Sergey Krapivenskiy, 16-03-17 21:03:16
 
 Creates NSDictionary from JSON object on disk
 
 @param filePath JSON file path
 @param error    NSError
 
 @return NSDictionary
 */
+ (instancetype)dictionaryFromJSONFile:(NSString *)filePath
                             withError:(NSError **)error;

@end
