//
//  NSDictionary+ALSJSONFromFile.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "NSDictionary+ALSJSONFromFile.h"

@implementation NSDictionary (ALSJSONFromFile)

+ (instancetype)dictionaryFromJSONFile:(NSString *)filePath
                             withError:(NSError **)error {
    
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    
    NSData *fileContents = [NSData dataWithContentsOfURL:fileURL
                                                 options:0
                                                   error:error];
    if (error != nil && *error != nil) {
        return nil;
    }
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:fileContents
                                                           options:0
                                                             error:error];
    return result;
}

@end