//
//  TestConstants.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#ifndef TestConstants_h
#define TestConstants_h

#import <Foundation/Foundation.h>

extern NSTimeInterval ALSDefaultTestTimeout;

#endif /* TestConstants_h */
