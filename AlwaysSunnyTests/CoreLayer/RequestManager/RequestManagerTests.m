//
//  RequestManagerTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "RequestManagerBase.h"
#import "OCMock.h"

#import "NetworkClient.h"
#import "ResponseDeserializer.h"
#import "ResponseValidator.h"
#import "ResponseMapper.h"
#import "URLRequestConfigurator.h"

#import "TestConstants.h"

typedef void(^RequestManagerExpectationBlock)(NSArray *objects, NSError *error);

@interface RequestManagerTests : XCTestCase

@property (nonatomic, strong) RequestManagerBase *requestManager;

@end

@implementation RequestManagerTests

#pragma mark - Lifecycle

- (void)setUp {
    [super setUp];
    self.requestManager = [RequestManagerBase new];
    self.requestManager.requestConfigurator = [self requestConfiguratorMock];
    
    // By default all dependencies are substituted by success mocks
    self.requestManager.networkClient = [self clientMockWithData:[NSData new]
                                                       error:nil];
    self.requestManager.responseDeserializer = [self deserializerMockWithResponse:@{}
                                                                            error:nil];
    self.requestManager.responseValidator = [self validatorMockWithError:nil];
    self.requestManager.responseMapper = [self mapperMockWithResult:[self genericMappingResult]
                                                              error:nil];
}

- (void)tearDown {
    self.requestManager = nil;
    [super tearDown];
}

#pragma mark - Test

- (void)testThatManagerReturnsErrorForClientError {
    
    // given
    XCTestExpectation *expectation = [self expectationWithDescription:@"client error expectation"];
    NSError *expectedError = [self genericError];
    self.requestManager.networkClient = [self clientMockWithData:nil
                                                       error:expectedError];
    
    // then
    [self performRequestManagerTestWithExpectation:expectation
                                  expectationBlock:^(NSArray *objects, NSError *error)
     {
         XCTAssertNil(objects);
         XCTAssertEqualObjects(error, expectedError);
     }];
}

- (void)testThatManagerReturnsErrorForDeserializerError {
    
    // given
    XCTestExpectation *expectation = [self expectationWithDescription:@"deserializer error expectation"];
    NSError *expectedError = [self genericError];
    self.requestManager.responseDeserializer = [self deserializerMockWithResponse:nil
                                                                            error:expectedError];
    
    // then
    [self performRequestManagerTestWithExpectation:expectation
                                  expectationBlock:^(NSArray *objects, NSError *error)
     {
         XCTAssertNil(objects);
         XCTAssertEqualObjects(error, expectedError);
     }];
}

- (void)testThatManagerReturnsErrorForValidatorError {
    
    // given
    XCTestExpectation *expectation = [self expectationWithDescription:@"validator error expectation"];
    NSError *expectedError = [self genericError];
    self.requestManager.responseValidator = [self validatorMockWithError:expectedError];
    
    // then
    [self performRequestManagerTestWithExpectation:expectation
                                  expectationBlock:^(NSArray *objects, NSError *error)
     {
         XCTAssertNil(objects);
         XCTAssertEqualObjects(error, expectedError);
     }];
}

- (void)testThatManagerReturnsErrorForMapperError {
    
    // given
    XCTestExpectation *expectation = [self expectationWithDescription:@"mapper error expectation"];
    NSError *expectedError = [self genericError];
    self.requestManager.responseMapper = [self mapperMockWithResult:nil
                                                              error:expectedError];;
    
    // then
    [self performRequestManagerTestWithExpectation:expectation
                                  expectationBlock:^(NSArray *objects, NSError *error)
     {
         XCTAssertNil(objects);
         XCTAssertEqualObjects(error, expectedError);
     }];
}

- (void)testThatManagerSucceeds {
    
    // given
    NSArray *expectedResults = [self genericMappingResult];
    XCTestExpectation *expectation = [self expectationWithDescription:@"success expectation"];
    self.requestManager.responseMapper = [self mapperMockWithResult:expectedResults
                                                              error:nil];
    
    // then
    [self performRequestManagerTestWithExpectation:expectation
                                  expectationBlock:^(NSArray *objects, NSError *error)
     {
         XCTAssertEqualObjects(objects, expectedResults);
         XCTAssertNil(error);
     }];
}

#pragma mark - Вспомогательные методы

- (void)performRequestManagerTestWithExpectation:(XCTestExpectation *)expectation
                                expectationBlock:(RequestManagerExpectationBlock)block {
    
    __block NSArray *receivedObjects = nil;
    __block NSError *receivedError = nil;
    
    // when
    [self.requestManager performRequestWithMethod:@"SomeMethod"
                                         urlParts:nil
                                     requestModel:nil
                                   mappingContext:nil
                                       completion:^(NSArray *objects, NSError *error) {
        receivedObjects = objects;
        receivedError = error;
        dispatch_async(dispatch_get_main_queue(), ^{
            [expectation fulfill];
        });
    }];
    
    // then
    [self waitForExpectationsWithTimeout:ALSDefaultTestTimeout
                                 handler:^(NSError *error)
     {
         block(receivedObjects, receivedError);
     }];
}

- (id)requestConfiguratorMock {
    
    id requestConfiguratorMock = OCMStrictProtocolMock(@protocol(URLRequestConfigurator));
    OCMStub([requestConfiguratorMock requestWithMethod:[OCMArg any]
                                              urlParts:[OCMArg any]
                                          requestModel:[OCMArg any]])
    .andReturn([NSURLRequest new]);
    
    return requestConfiguratorMock;
}

- (id)clientMockWithData:(id)data
                   error:(NSError *)error {
    
    id clientMock = OCMStrictProtocolMock(@protocol(NetworkClient));
    OCMStub([clientMock performRequest:[OCMArg any]
                        withCompletion:[OCMArg any]])
    .andDo(^(NSInvocation *invocation) {
        [invocation retainArguments];
        NetworkClientCompletionBlock completionBlock;
        [invocation getArgument:&completionBlock atIndex:3];
        [completionBlock copy];
        completionBlock(data, error);
    });
    
    return clientMock;
}

- (id)deserializerMockWithResponse:(NSDictionary *)response
                             error:(NSError *)error {
    
    id deserializerMock = OCMStrictProtocolMock(@protocol(ResponseDeserializer));
    OCMStub([deserializerMock deserializeServerResponse:[OCMArg any]
                                        completionBlock:[OCMArg any]])
    .andDo(^(NSInvocation *invocation) {
        [invocation retainArguments];
        DeserializerCompletionBlock completionBlock;
        [invocation getArgument:&completionBlock atIndex:3];
        [completionBlock copy];
        completionBlock(response, error);
    });
    
    return deserializerMock;
}

- (id)validatorMockWithError:(NSError *)error {
    
    id validatorMock = OCMStrictProtocolMock(@protocol(ResponseValidator));
    OCMStub([validatorMock validateServerResponse:[OCMArg any]]).andReturn(error);
    
    return validatorMock;
}

- (id)mapperMockWithResult:(id)mappedObject
                     error:(NSError *)error {
    
    id mapperMock = OCMStrictProtocolMock(@protocol(ResponseMapper));
    OCMStub([mapperMock mapSourceObject:[OCMArg any]
                     withMappingContext:[OCMArg any]
                                  error:[OCMArg anyObjectRef]])
    .andDo(^(NSInvocation *invocation) {
        [invocation retainArguments];
        NSError *__autoreleasing *argError;
        [invocation getArgument:&argError atIndex:4];
        *argError = error;
    }).andReturn(mappedObject);
    
    return mapperMock;
}

- (NSArray *)genericMappingResult {
    
    return @[ @"One", @"Two" ];
}

- (NSError *)genericError {
    NSError *error = [NSError errorWithDomain:@"Test Domain"
                                         code:0
                                     userInfo:@{ NSLocalizedDescriptionKey : @"Lalala" }];
    
    return error;
}

@end