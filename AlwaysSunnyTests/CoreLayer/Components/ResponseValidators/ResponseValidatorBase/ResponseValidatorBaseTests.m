//
//  ResponseValidatorBaseTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "ResponseValidatorBase.h"

@interface ResponseValidatorBaseTests : XCTestCase

@property (nonatomic, strong) ResponseValidatorBase *validator;

@end

@implementation ResponseValidatorBaseTests

- (void)setUp {
    [super setUp];
    self.validator = [ResponseValidatorBase new];
}

- (void)tearDown {
    self.validator = nil;
    [super tearDown];
}

#pragma mark - Tests

- (void)testThatDictionaryPassesValidation {
    // given
    NSDictionary *response = @{ @"key" : @"value" };
    NSError *validationError = nil;
    
    // when
    BOOL result = [self.validator validateResponseIsDictionary:response
                                           error:&validationError];
    
    // then
    XCTAssertTrue(result);
    XCTAssertNil(validationError);
}

- (void)testThatRandomDataDoesntPassValidation {
    // given
    NSArray *array = @[ @1, @2 ];
    NSError *validationError = nil;
    
    // when
    BOOL result = [self.validator validateResponseIsDictionary:array
                                                         error:&validationError];
    
    // then
    XCTAssertFalse(result);
    XCTAssertNotNil(validationError);
}

@end