//
//  SearchResponseValidatorTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "SearchResponseValidator.h"
#import "APIKeys.h"

@interface SearchResponseValidatorTests : XCTestCase

@property (nonatomic, strong) SearchResponseValidator *validator;

@end

@implementation SearchResponseValidatorTests

#pragma mark - Lifecycle

- (void)setUp {
    [super setUp];
    self.validator = [SearchResponseValidator new];
}

- (void)tearDown {
    self.validator = nil;
    [super tearDown];
}

#pragma mark - Tests

- (void)testThatCorrectResponsePassesValidation {
    //given
    NSDictionary *resultResponse = @{ ALSResultKey : @[ @1 ] };
    
    NSDictionary *response = @{ ALSSearchAPIKey : resultResponse };
    
    //when
    NSError *validationError = [self.validator validateServerResponse:response];
    
    //then
    XCTAssertNil(validationError);
}

- (void)testThatResponseWithoutSearchAPIFailsValidation {
    //given
    NSDictionary *resultResponse = @{ @"lorem" : @"ipsum" };
    
    NSDictionary *response = @{ @"random" : resultResponse };
    
    //when
    NSError *validationError = [self.validator validateServerResponse:response];
    
    //then
    XCTAssertNotNil(validationError);
}

- (void)testThatResponseWithoutResultFailsValidation {
    //given
    NSDictionary *resultResponse = @{ @"lorem" : @"ipsum" };
    
    NSDictionary *response = @{ ALSSearchAPIKey : resultResponse };
    
    //when
    NSError *validationError = [self.validator validateServerResponse:response];
    
    //then
    XCTAssertNotNil(validationError);
}

- (void)testThatResponseWithErrorFailsValidation {
    //given
    NSDictionary *resultResponse = @{ ALSErrorKey : @"some error" };
    
    NSDictionary *response = @{ ALSDataKey : resultResponse };
    
    //when
    NSError *validationError = [self.validator validateServerResponse:response];
    
    //then
    XCTAssertNotNil(validationError);
}

- (void)testThatValidationErrorPreservesErrorData {
    //given
    NSString *errorDescription = @"Everything is not ok";
    NSDictionary *error = @{ ALSErrorMessageKey : errorDescription };
    NSDictionary *errorResponse = @{ ALSErrorKey : @[ error ] };
    
    NSDictionary *response = @{ ALSDataKey : errorResponse };
    
    NSDictionary *expectedResponse = @{ ALSResponseKey : response };
    
    //when
    NSError *validationError = [self.validator validateServerResponse:response];
    
    //then
    XCTAssertEqualObjects(validationError.userInfo, expectedResponse);
}

@end