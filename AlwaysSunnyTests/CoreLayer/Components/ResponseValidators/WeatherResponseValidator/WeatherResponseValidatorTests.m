//
//  WeatherResponseValidatorTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "WeatherResponseValidator.h"
#import "APIKeys.h"

@interface WeatherResponseValidatorTests : XCTestCase

@property (nonatomic, strong) WeatherResponseValidator *validator;

@end

@implementation WeatherResponseValidatorTests

#pragma mark - Lifecycle

- (void)setUp {
    [super setUp];
    self.validator = [WeatherResponseValidator new];
}

- (void)tearDown {
    self.validator = nil;
    [super tearDown];
}

#pragma mark - Tests

- (void)testThatCorrectResponsePassesValidation {
    //given
    NSDictionary *resultResponse = @{ ALSCurrentConditionKey : @[ @1 ] };
    
    NSDictionary *response = @{ ALSDataKey : resultResponse };
    
    //when
    NSError *validationError = [self.validator validateServerResponse:response];
    
    //then
    XCTAssertNil(validationError);
}

- (void)testThatResponseWithoutDataFailsValidation {
    //given
    NSDictionary *resultResponse = @{ @"lorem" : @"ipsum" };
    
    NSDictionary *response = @{ @"random" : resultResponse };
    
    //when
    NSError *validationError = [self.validator validateServerResponse:response];
    
    //then
    XCTAssertNotNil(validationError);
}

- (void)testThatResponseWithoutCurrentConditionFailsValidation {
    //given
    NSDictionary *resultResponse = @{ @"lorem" : @"ipsum" };
    
    NSDictionary *response = @{ ALSDataKey : resultResponse };
    
    //when
    NSError *validationError = [self.validator validateServerResponse:response];
    
    //then
    XCTAssertNotNil(validationError);
}

- (void)testThatResponseWithErrorFailsValidation {
    //given
    NSDictionary *resultResponse = @{ ALSErrorKey : @"some error" };
    
    NSDictionary *response = @{ ALSDataKey : resultResponse };
    
    //when
    NSError *validationError = [self.validator validateServerResponse:response];
    
    //then
    XCTAssertNotNil(validationError);
}

- (void)testThatValidationErrorPreservesErrorData {
    //given
    NSString *errorDescription = @"Everything is not ok";
    NSDictionary *error = @{ ALSErrorMessageKey : errorDescription };
    NSDictionary *errorResponse = @{ ALSErrorKey : @[ error ] };
    
    NSDictionary *response = @{ ALSDataKey : errorResponse };
    
    NSDictionary *expectedResponse = @{ ALSResponseKey : response };
    
    //when
    NSError *validationError = [self.validator validateServerResponse:response];
    
    //then
    XCTAssertEqualObjects(validationError.userInfo, expectedResponse);
}

@end