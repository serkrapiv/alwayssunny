//
//  URLRequestConfiguratorBaseTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "RequestModel.h"
#import "URLRequestConfiguratorBase.h"

static NSString *const ALSTestGETMethod = @"GET";
static NSString *const ALSTestBaseURLString = @"http://google.com";

@interface URLRequestConfiguratorBaseTests : XCTestCase

@property (nonatomic, strong) URLRequestConfiguratorBase *configurator;

@end

@implementation URLRequestConfiguratorBaseTests

#pragma mark - Lifecycle

- (void)setUp {
    [super setUp];
    NSURL *baseURL = [NSURL URLWithString:ALSTestBaseURLString];
    self.configurator = [[URLRequestConfiguratorBase alloc] initWithBaseURL:baseURL];
}

- (void)tearDown {
    self.configurator = nil;
    [super tearDown];
}

#pragma mark - Tests

- (void)testThatConfiguratorCreatesRequestWithoutParameters {
    
    // given
    NSString *method = ALSTestGETMethod;
    NSString *urlPart = @"@me";
    NSString *expectedString = @"http://google.com/@me";
    
    // when
    NSURLRequest *request = [self.configurator requestWithMethod:method
                                                        urlParts:@[ urlPart ]
                                                    requestModel:nil];
    NSString *resultURLString = request.URL.absoluteString;
    
    // then
    XCTAssertEqualObjects(resultURLString, expectedString);
    XCTAssertEqualObjects(request.HTTPMethod, method);
}

- (void)testThatConfiguratorCreatesRequestWithQueryParameters {
    
    // given
    NSString *method = ALSTestGETMethod;
    NSString *urlPart = @"getmesome";
    
    RequestModel *requestModel = [RequestModel new];
    requestModel.queryParameters = @{
                                     @"key" : @"value"
                                     };
    NSString *expectedString = @"http://google.com/getmesome?key=value";
    
    // when
    NSURLRequest *request = [self.configurator requestWithMethod:method
                                                        urlParts:@[ urlPart ]
                                                    requestModel:requestModel];
    NSString *resultURLString = request.URL.absoluteString;
    
    // then
    XCTAssertEqualObjects(resultURLString, expectedString);
}

- (void)testThatConfiguratorCreatesRequestWithHeaderFields {
    
    // given
    NSString *method = ALSTestGETMethod;
    NSString *urlPart = @"auth";
    
    RequestModel *requestModel = [RequestModel new];
    NSDictionary *headerFields = @{
                                   @"Content-Length" : @"230"
                                   };
    requestModel.httpHeaderFields = headerFields;
    
    NSString *expectedString = @"http://google.com/auth";
    
    // when
    NSURLRequest *request = [self.configurator requestWithMethod:method
                                                        urlParts:@[ urlPart ]
                                                    requestModel:requestModel];
    NSString *resultURLString = request.URL.absoluteString;
    
    // then
    XCTAssertEqualObjects(resultURLString, expectedString);
    XCTAssertEqualObjects(request.allHTTPHeaderFields, headerFields);
}

@end