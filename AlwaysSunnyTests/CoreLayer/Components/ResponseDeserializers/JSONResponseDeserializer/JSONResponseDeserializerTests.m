//
//  JSONResponseDeserializerTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "JSONResponseDeserializer.h"
#import "TestConstants.h"

@interface JSONResponseDeserializerTests : XCTestCase

@property (nonatomic, strong) JSONResponseDeserializer *deserializer;

@end

@implementation JSONResponseDeserializerTests

#pragma mark - Lifecycle

- (void)setUp {
    [super setUp];
    self.deserializer = [JSONResponseDeserializer new];
}

- (void)tearDown {
    self.deserializer = nil;
    [super tearDown];
}

#pragma mark - Tests

- (void)testThatDeserializerDeserializesNoCriminality {
    // given
    XCTestExpectation *expectation = [self expectationWithDescription:@"json deserializer expectation"];
    NSData *data = [NSJSONSerialization dataWithJSONObject:@{
                                                             @"key" : @"value"
                                                             }
                                                   options:0
                                                     error:nil];
    __block NSDictionary *result;
    
    // when
    [self.deserializer deserializeServerResponse:data
                                 completionBlock:^(NSDictionary *response, NSError *error) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         result = response;
                                         [expectation fulfill];
                                     });
                                 }];
    
    // then
    [self waitForExpectationsWithTimeout:ALSDefaultTestTimeout
                                 handler:^(NSError *error) {
        XCTAssertNotNil(result);
        XCTAssertEqualObjects([[result allKeys] firstObject], @"key");
        XCTAssertEqualObjects([[result allValues] firstObject], @"value");
    }];
}

- (void)testNilResponse {
    // given
    XCTestExpectation *expectation = [self expectationWithDescription:@"json deserializer nil expectation"];
    NSData *data = nil;
    __block NSDictionary *result;
    
    // when
    [self.deserializer deserializeServerResponse:data
                                 completionBlock:^(NSDictionary *response, NSError *error) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         result = response;
                                         [expectation fulfill];
                                     });
                                 }];
    
    // then
    [self waitForExpectationsWithTimeout:ALSDefaultTestTimeout
                                 handler:^(NSError *error) {
        XCTAssertNil(result);
    }];
}

@end