//
//  RequestSignerBaseTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "RequestSignerBase.h"
#import "APIKeys.h"

static NSString *const ALSTestKey = @"testKey";

@interface RequestSignerBaseTests : XCTestCase

@property (nonatomic, strong) RequestSignerBase *requestSigner;

@end

@implementation RequestSignerBaseTests

#pragma mark - Lifecycle

- (void)setUp {
    [super setUp];
    self.requestSigner = [RequestSignerBase new];
    self.requestSigner.APIKey = ALSTestKey;
}

- (void)tearDown {
    self.requestSigner = nil;
    [super tearDown];
}

#pragma mark - Tests

- (void)testThatRequestHasKey {
    // given
    NSURLRequest *request = [self request];
    NSString *expectedString = [NSString stringWithFormat:@"%@=%@", ALSWeatherAPIKeyKey, ALSTestKey];
    
    // when
    NSURLRequest *signedRequest = [self.requestSigner signRequest:request];

    // then
    XCTAssertTrue([signedRequest.URL.absoluteString containsString:expectedString]);
}

#pragma mark - Helpers

- (NSURLRequest *)request {
    NSMutableURLRequest *request = [[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]] mutableCopy];
    
    NSDictionary *parameters = @{ @"format" : @"json",
                                  @"query" : @"zzz" };
    
    NSData *requestBody = [NSJSONSerialization dataWithJSONObject:parameters
                                                          options:0
                                                            error:nil];
    
    request.HTTPBody = requestBody;
    return request;
}

@end
