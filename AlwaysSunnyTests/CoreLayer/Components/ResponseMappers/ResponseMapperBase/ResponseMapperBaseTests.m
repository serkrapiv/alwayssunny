//
//  ResponseMapperBaseTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "ResponseMapperBase.h"
#import "MappingContext.h"
#import "MappingType.h"
#import "MappingProviderBase.h"

#import "NSDictionary+ALSJSONFromFile.h"

#import "DailyForecast.h"
#import "City.h"

@interface ResponseMapperBaseTests : XCTestCase

@property (nonatomic, strong) ResponseMapperBase *mapper;

@end

@implementation ResponseMapperBaseTests

#pragma mark - Lifecycle

- (void)setUp {
    [super setUp];
    self.mapper = [ResponseMapperBase new];
    MappingProviderBase *provider = [MappingProviderBase new];
    self.mapper.mappingProvider = provider;
}

- (void)tearDown {
    self.mapper = nil;
    [super tearDown];
}

#pragma mark - Tests

- (void)testThatDailyForecastIsMapped {
    // given
    NSDictionary *sourceObject = @{ @"date" : [NSDate date],
                                    @"astronomy" : @{ @"sunrise" : @"06:00 AM",
                                                      @"sunset" : @"07:00 PM" },
                                    @"maxtempC": @"6",
                                    @"maxtempF": @"44",
                                    @"mintempC": @"-2",
                                    @"mintempF": @"29",
                                    @"uvIndex": @"2" };
    MappingContext *context = [MappingContext new];
    context.mappingType = ALSMappingTypeWeather;
    context.keyPath = nil;
    NSError *error = nil;
    
    // when
    id mappedObject = [self.mapper mapSourceObject:sourceObject
                                withMappingContext:context
                                             error:&error];
    
    // then
    XCTAssertTrue([mappedObject isKindOfClass:[DailyForecast class]]);
    XCTAssertNil(error);
}

- (void)testThatMultipleForecastsAreMapped {
    // given
    NSDictionary *sourceObject = [self jsonObjectFromFile:@"forecasts.json"];
    
    MappingContext *context = [MappingContext new];
    context.mappingType = ALSMappingTypeWeather;
    context.keyPath = @"data.weather";
    NSError *error = nil;
    
    // when
    NSArray *mappingResult = [self.mapper mapSourceObject:sourceObject
                                 withMappingContext:context
                                              error:&error];
    
    // then
    XCTAssertTrue([mappingResult isKindOfClass:[NSArray class]]);
    XCTAssertTrue([mappingResult.firstObject isKindOfClass:[DailyForecast class]]);
    
    XCTAssertNil(error);
}

- (void)testThatMultipleCitiesAreMapped {
    // given
    NSDictionary *sourceObject = [self jsonObjectFromFile:@"cities.json"];
    
    MappingContext *context = [MappingContext new];
    context.mappingType = ALSMappingTypeSearchResult;
    context.keyPath = @"search_api.result";
    NSError *error = nil;
    
    // when
    NSArray *mappingResult = [self.mapper mapSourceObject:sourceObject
                                       withMappingContext:context
                                                    error:&error];
    
    // then
    XCTAssertTrue([mappingResult isKindOfClass:[NSArray class]]);
    XCTAssertTrue([mappingResult.firstObject isKindOfClass:[City class]]);
    
    XCTAssertNil(error);
}

#pragma mark - Helpers

- (NSDictionary *)jsonObjectFromFile:(NSString *)fileName {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *filePath = [bundle pathForResource:[fileName stringByDeletingPathExtension] ofType:[fileName pathExtension]];
    NSDictionary *result = [NSDictionary dictionaryFromJSONFile:filePath withError:nil];
    return result;
}

@end
