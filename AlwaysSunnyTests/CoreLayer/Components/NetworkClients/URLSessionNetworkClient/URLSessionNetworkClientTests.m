//
//  URLSessionNetworkClientTests.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "OCMock.h"
#import "OHHTTPStubs.h"

#import "URLSessionNetworkClient.h"

#import "TestConstants.h"

static NSString *ALSRequestTestUrlString = @"http://google.com/requesttest";

@interface URLSessionNetworkClientTests : XCTestCase

@property (nonatomic, strong) URLSessionNetworkClient *client;

@end

@implementation URLSessionNetworkClientTests

#pragma mark - Lifecycle

- (void)setUp {
    [super setUp];
    self.client = [URLSessionNetworkClient new];
}

- (void)tearDown {
    self.client = nil;
    [super tearDown];
}

#pragma mark - Tests

- (void)testThatClientSendsRequest {
    // given
    id mockSession = OCMClassMock([NSURLSession class]);
    self.client.session = mockSession;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:ALSRequestTestUrlString]];
    
    // when
    [self.client performRequest:request withCompletion:nil];
    
    // then
    OCMVerify([mockSession dataTaskWithRequest:request
                             completionHandler:OCMOCK_ANY]);
    [mockSession stopMocking];
}

- (void)testThatNilRequestThrowsException {
    
    //given
    NSURLRequest *nilRequest = nil;
    
    //when and then
    XCTAssertThrows([self.client performRequest:nilRequest withCompletion:nil]);
}

- (void)testThatSuccessfulRequestReturnsData {
    
    //given
    XCTestExpectation *expectation = [self expectationWithDescription:@"request success expectation"];
    self.client.session = [NSURLSession sharedSession];
    
    NSString *stringForData = @"some string";
    NSData *responseData = [stringForData dataUsingEncoding:NSUTF8StringEncoding];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return YES;
        return [request.URL.absoluteString isEqualToString:ALSRequestTestUrlString];
    } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
        
        return [OHHTTPStubsResponse responseWithData:responseData
                                          statusCode:200
                                             headers:@{@"Content-Type":@"application/json"}];
        
    }];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:ALSRequestTestUrlString]];
    
    __block NSError *receivedError = nil;
    __block NSData *receivedData = nil;
    //when
    [self.client performRequest:request withCompletion:^(id data, NSError *error) {
        receivedError = error;
        receivedData = data;
        [expectation fulfill];
    }];
    
    //then
    [self waitForExpectationsWithTimeout:ALSDefaultTestTimeout
                                 handler:^(NSError * _Nullable error)
     {
         XCTAssertNil(receivedError);
         XCTAssertEqualObjects(receivedData, responseData);
     }];
}

- (void)testThatFailedRequestReturnsError {
    
    //given
    XCTestExpectation *expectation = [self expectationWithDescription:@"request fail expectation"];
    self.client.session = [NSURLSession sharedSession];
    
    NSError *responseError = [[NSError alloc] initWithDomain:@"ALS Domain"
                                                        code:404
                                                    userInfo:@{ NSLocalizedDescriptionKey : @"some error"}];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [request.URL.absoluteString isEqualToString:ALSRequestTestUrlString];
    } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
        
        return [OHHTTPStubsResponse responseWithError:responseError];
        
    }];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:ALSRequestTestUrlString]];
    
    __block NSError *receivedError = nil;
    
    //when
    [self.client performRequest:request withCompletion:^(id data, NSError *error) {
        receivedError = error;
        [expectation fulfill];
    }];
    
    //then
    [self waitForExpectationsWithTimeout:ALSDefaultTestTimeout
                                 handler:^(NSError * _Nullable error)
     {
         XCTAssertNotNil(receivedError);
         XCTAssertEqualObjects(receivedError, responseError);
     }];
}
@end
