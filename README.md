# README #

A simple iPhone weather forecast application. It allows user to select a city and get a 5 day forecast for the selected city.

The user is able to add and remove cities, and cities list is persisted.