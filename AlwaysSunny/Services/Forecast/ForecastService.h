//
//  ForecastService.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceCompletionBlocks.h"

/**
 @author Sergey Krapivenskiy, 16-03-18 11:03:48
 
 This service is responsible for loading weather data
 */
@protocol ForecastService <NSObject>

- (void)loadForecastsForCityWithName:(NSString *)cityName
                        numberOfDays:(NSUInteger)numberOfDays
                          completion:(ServiceCompletionBlockWithArrayOfObjects)completion;

@end
