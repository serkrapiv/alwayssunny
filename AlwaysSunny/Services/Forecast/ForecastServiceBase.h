//
//  ForecastServiceBase.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ForecastService.h"

@protocol RequestManager;

@interface ForecastServiceBase : NSObject <ForecastService>

@property (nonatomic, strong) id<RequestManager> requestManager;

@end
