//
//  ForecastServiceBase.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "ForecastServiceBase.h"
#import "RequestManager.h"

#import "MappingContext.h"
#import "RequestModel.h"

#import "APIKeys.h"
#import "APIMethods.h"

@implementation ForecastServiceBase

- (void)loadForecastsForCityWithName:(NSString *)cityName
                        numberOfDays:(NSUInteger)numberOfDays
                          completion:(ServiceCompletionBlockWithArrayOfObjects)completion {
    
    NSDictionary *parameters = @{ ALSWeatherQueryKey : cityName,
                                  ALSFormatKey : @"json",
                                  ALSNumberOfDaysKey : @(numberOfDays) };
    
    RequestModel *requestModel = [RequestModel new];
    requestModel.queryParameters = parameters;
    
    MappingContext *mappingContext = [MappingContext new];
    mappingContext.mappingType = ALSMappingTypeWeather;
    mappingContext.keyPath = @"data.weather";
    
    [self.requestManager performRequestWithMethod:ALSHTTPMethodGET
                                         urlParts:@[ALSWeatherMethodName]
                                     requestModel:requestModel
                                   mappingContext:mappingContext
                                       completion:^(NSArray *objects, NSError *error) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               completion(objects, error);
                                           });
                                       }];
}

@end