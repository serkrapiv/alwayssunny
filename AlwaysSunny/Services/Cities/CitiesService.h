//
//  CitiesService.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceCompletionBlocks.h"

@class City;

/**
 @author Sergey Krapivenskiy, 16-03-18 11:03:20
 
 This service is responsible for all operations with cities, like loading data from backend or saving selected cities on the device
 */
@protocol CitiesService <NSObject>

- (NSArray *)obtainSavedCities;

- (void)loadCitiesForSearchString:(NSString *)searchString
                   withCompletion:(ServiceCompletionBlockWithArrayOfObjects)completionBlock;

- (void)addCity:(City *)city;

- (void)removeCity:(City *)city;

@end
