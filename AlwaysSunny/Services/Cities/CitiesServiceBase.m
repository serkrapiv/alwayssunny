//
//  CitiesServiceBase.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "CitiesServiceBase.h"

#import "MappingContext.h"
#import "RequestModel.h"

#import "RequestManager.h"

#import "APIMethods.h"
#import "APIKeys.h"

NSString *const ALSSavedCitiesKey = @"als.saved.cities";

@implementation CitiesServiceBase

#pragma mark - CitiesService

- (NSArray *)obtainSavedCities {
    NSData *citiesData = [self.userDefaults objectForKey:ALSSavedCitiesKey];
    if (!citiesData) return nil;
    
    NSArray *cities = [NSKeyedUnarchiver unarchiveObjectWithData:citiesData];
    
    return cities;
}

- (void)loadCitiesForSearchString:(NSString *)searchString
                     withCompletion:(ServiceCompletionBlockWithArrayOfObjects)completionBlock {
    NSDictionary *parameters = @{ ALSSearchQueryKey : searchString,
                                  ALSFormatKey : @"json" };
    
    RequestModel *requestModel = [RequestModel new];
    requestModel.queryParameters = parameters;
    
    MappingContext *mappingContext = [MappingContext new];
    mappingContext.mappingType = ALSMappingTypeSearchResult;
    mappingContext.keyPath = @"search_api.result";
    
    [self.requestManager performRequestWithMethod:ALSHTTPMethodGET
                                         urlParts:@[ALSSearchMethodName]
                                     requestModel:requestModel
                                   mappingContext:mappingContext
                                       completion:^(NSArray *objects, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(objects, error);
        });
    }];
}

- (void)addCity:(City *)city {
    NSArray *loadedCities = [self obtainSavedCities];
    NSMutableArray *citiesToSave = [NSMutableArray arrayWithArray:loadedCities];
    [citiesToSave addObject:city];
    [self saveCities:citiesToSave];
}

- (void)removeCity:(City *)city {
    NSMutableArray *cities = [[self obtainSavedCities] mutableCopy];
    [cities removeObject:city];
    [self saveCities:cities];
}

#pragma mark - Private

- (void)saveCities:(NSArray *)cities {
    NSData *archivedCities = [NSKeyedArchiver archivedDataWithRootObject:cities];
    
    [self.userDefaults setObject:archivedCities forKey:ALSSavedCitiesKey];
    [self.userDefaults synchronize];
}

@end