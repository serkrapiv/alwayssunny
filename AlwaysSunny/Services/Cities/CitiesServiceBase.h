//
//  CitiesServiceBase.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CitiesService.h"

@protocol RequestManager;

extern NSString *const ALSSavedCitiesKey;

@interface CitiesServiceBase : NSObject <CitiesService>

@property (nonatomic, strong) id<RequestManager> requestManager;
@property (nonatomic, strong) NSUserDefaults *userDefaults;

@end