//
//  ServicesAssembly.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "ServicesAssembly.h"
#import "CoreLayerAssembly.h"

#import "CitiesServiceBase.h"
#import "ForecastServiceBase.h"

#import "RequestManagerBase.h"

@interface ServicesAssembly ()

@property (nonatomic, strong) CoreLayerAssembly *coreLayerAssembly;

@end

@implementation ServicesAssembly

#pragma mark - Services

- (id<CitiesService>)citiesService {
    return [TyphoonDefinition withClass:[CitiesServiceBase class]
                          configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(requestManager)
                              with:[self citiesRequestManager]];
        [definition injectProperty:@selector(userDefaults)
                              with:[NSUserDefaults standardUserDefaults]];
    }];
}

- (id<ForecastService>)forecastService {
    return [TyphoonDefinition withClass:[ForecastServiceBase class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(requestManager)
                                      with:[self forecastRequestManager]];
            }];
}

#pragma mark - Request managers

- (id<RequestManager>)baseRequestManager {
    return [TyphoonDefinition withClass:[RequestManagerBase class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(networkClient)
                                      with:[self.coreLayerAssembly networkClient]];
                [definition injectProperty:@selector(requestConfigurator)
                                      with:[self.coreLayerAssembly requestConfigurator]];
                [definition injectProperty:@selector(responseDeserializer)
                                      with:[self.coreLayerAssembly responseDeserializer]];
                [definition injectProperty:@selector(responseMapper)
                                      with:[self.coreLayerAssembly responseMapper]];
                [definition injectProperty:@selector(requestSigner)
                                      with:[self.coreLayerAssembly requestSigner]];
            }];
}

- (id<RequestManager>)citiesRequestManager {
    return [TyphoonDefinition withParent:[self baseRequestManager]
                                   class:[RequestManagerBase class]
                           configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(responseValidator)
                              with:[self.coreLayerAssembly searchResponseValidator]];
    }];
}

- (id<RequestManager>)forecastRequestManager {
    return [TyphoonDefinition withParent:[self baseRequestManager]
                                   class:[RequestManagerBase class]
                           configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(responseValidator)
                                      with:[self.coreLayerAssembly weatherResponseValidator]];
            }];
}

@end
