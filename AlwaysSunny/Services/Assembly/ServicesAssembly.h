//
//  ServicesAssembly.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@protocol CitiesService;
@protocol ForecastService;

@interface ServicesAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (id<CitiesService>)citiesService;

- (id<ForecastService>)forecastService;

@end
