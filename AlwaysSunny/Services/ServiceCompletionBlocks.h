//
//  ServiceCompletionBlocks.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#ifndef ServiceCompletionBlocks_h
#define ServiceCompletionBlocks_h

/**
 @author Sergey Krapivenskiy, 16-03-17 20:03:51
 
 Used for services that return an array of objects or an error
 
 @param objects Received objects
 @param error   Error
 */
typedef void (^ServiceCompletionBlockWithArrayOfObjects)(NSArray *objects, NSError *error);

#endif /* ServiceCompletionBlocks_h */
