//
//  NSMutableArray+RDSNilFilter.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 16/06/15.
//  Copyright (c) 2015 Rambler DS. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @author Сергей Крапивенский, 16-03-18 13:03:08
 
 Allows for safe adding to NSMutableArray
 */
@interface NSMutableArray (RDSNilFilter)

/**
 @author Sergey Krapivenskiy, 15-10-28 18:10:49
 
 Adds object if it's not empty
 */
- (void)rds_addNonNilObject:(id)object;

/**
 @author Sergey Krapivenskiy, 15-10-28 18:10:13
 
 Inserts object if it is not empty
 */
- (void)rds_insertNonNilObject:(id)object
                       atIndex:(NSUInteger)index;

/**
 @author Sergey Krapivenskiy, 16-03-18 13:03:28
 
 Adds only non-empty strings, ignores other objects
*/
- (void)rds_addString:(NSString *)string;

@end
