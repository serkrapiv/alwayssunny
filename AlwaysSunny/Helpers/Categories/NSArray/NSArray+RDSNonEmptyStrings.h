//
//  NSArray+RDSNonEmptyStrings.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 28/10/15.
//  Copyright © 2015 Rambler DS. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:29
 
 Concatenates non-empty components into an NSString. This category is used to create URLs
 */
@interface NSArray (RDSNonEmptyStrings)

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:36
 
 Concatenates non-nil strings and non-nil numbers, other objects are ignored. Separator string is added between objects
 
 @param separatorString Separator string
 
 @return NSString
 */
- (NSString *)rds_nonemptyComponentsJoinedByString:(NSString*)separatorString;

@end