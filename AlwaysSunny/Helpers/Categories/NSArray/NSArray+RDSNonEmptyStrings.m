//
//  NSArray+RDSNonEmptyStrings.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 28/10/15.
//  Copyright © 2015 Rambler DS. All rights reserved.
//

#import "NSArray+RDSNonEmptyStrings.h"

@implementation NSArray (RDSNonEmptyStrings)

- (NSString *)rds_nonemptyComponentsJoinedByString:(NSString *)joinString {
    
    if (!joinString) {
        joinString = @"";
    }
    
    NSMutableString *result = [NSMutableString new];
    
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if (([obj isKindOfClass: [NSString class]] && [obj length]) ||
            [obj isKindOfClass: [NSNumber class]]
            ) {
            
            [result appendFormat:@"%@%@", (result.length > 0)? joinString : @"", obj ];
        }
    }];
    
    return result.copy;
}

@end