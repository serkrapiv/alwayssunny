//
//  NSMutableDictionary+RDSNilFilter.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18.11.15.
//  Copyright © 2015 RAMBLER&Co. All rights reserved.
//

#import "NSMutableDictionary+RDSNilFilter.h"

@implementation NSMutableDictionary (RDSNilFilter)

- (void)rcc_addNonNilObject:(id)object forKey:(id<NSCopying>)key {
    if (object != nil && key != nil) {
        [self setObject:object forKey:key];
    }
}

@end
