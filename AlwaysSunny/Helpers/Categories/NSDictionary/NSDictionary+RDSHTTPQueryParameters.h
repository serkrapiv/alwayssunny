//
//  NSDictionary+RDSHTTPGET.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 19/05/15.
//  Copyright © 2015 Rambler DS. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @author Sergey Krapivenskiy, 16-03-18 13:03:04
 
 Transforms NSDictionary into HTTP query string
 */
@interface NSDictionary (RDSHTTPQueryParameters)

- (NSString *)rds_queryParametersStringEscaped:(BOOL)escaped;

@end
