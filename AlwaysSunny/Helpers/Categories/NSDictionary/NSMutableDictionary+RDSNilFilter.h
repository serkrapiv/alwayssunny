//
//  NSMutableDictionary+RDSNilFilter.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18.11.15.
//  Copyright © 2015 RAMBLER&Co. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (RDSNilFilter)

- (void)rcc_addNonNilObject:(id)object forKey:(id<NSCopying>)key;

@end
