//
//  NSDictionary+RDSHTTPGET.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 29/10/15.
//  Copyright © 2015 Rambler DS. All rights reserved.
//

#import "NSDictionary+RDSHTTPQueryParameters.h"
#import "NSString+RDSURLEncoding.h"

@interface NSString(HTTP_GET_HELPER)

+ (instancetype)rds_stringValueFromObject:(id)object
                          urlEncodeString:(BOOL)urlEncodedString;

@end

@implementation NSDictionary (RDSHTTPQueryParameters)

- (NSString *)rds_queryParametersStringEscaped:(BOOL)escaped {
    
    NSMutableDictionary *parsedFields = @{}.mutableCopy;
    
    [self enumerateKeysAndObjectsUsingBlock:^(NSString *key, id valueObj, BOOL *stop) {
        
        NSString *valueString = nil;
        
        if ([valueObj isKindOfClass: [NSArray class]]) {
            
            NSMutableString *arrayString = @"".mutableCopy;
            for (id obj in valueObj) {
                NSString *parameterString = [NSString rds_stringValueFromObject: obj urlEncodeString: escaped];
                if (parameterString != nil) {
                    [arrayString appendFormat: @"%@%@", (arrayString.length >0)? @"," : @"", (escaped ? [obj rds_urlEncodedString] : obj) ];
                }
            }
            
            valueString = arrayString.length != 0 ? arrayString.copy : nil;
            
        } else {
            
            valueString = [NSString rds_stringValueFromObject: valueObj urlEncodeString: escaped];
            
        }
        
        if (key != nil && valueString != nil) {
            parsedFields[ (escaped ? [key rds_urlEncodedString] : key) ] = valueString;
        }
    }];
    
    NSMutableString *result = @"".mutableCopy;
    
    NSArray *sortedKeys = [parsedFields.allKeys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare: obj2];
    }];
    
    [sortedKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger index, BOOL *stop) {
        
        [result appendFormat: @"%@%@=%@", (result.length == 0)? @"?" : @"&", key, parsedFields[key] ];
    }];
    
    
    return result;
}

@end

@implementation NSString(HTTP_GET_HELPER)


+ (instancetype)rds_stringValueFromObject: (id)object urlEncodeString: (BOOL)urlEncodedString {
    
    NSString *result = nil;
    
    if ([object isKindOfClass: [NSNumber class]]) {
        
        result = [NSString stringWithFormat: @"%@",object];
        
    } else if ([object isKindOfClass:[NSString class]]) {
        
        result = (urlEncodedString ? [object rds_urlEncodedString] : object);
        
    }
    
    return result;
}

@end
