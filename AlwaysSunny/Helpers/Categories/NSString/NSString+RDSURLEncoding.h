//
//  NSString+RDSURLEncoding.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/05/15.
//  Copyright © 2015 Rambler DS. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @author Sergey Krapivenskiy, 16-03-18 13:03:44
 
 Decoding and encoding for URL requests
 */
@interface NSString (RDSURLEncoding)

- (NSString*)rds_urlEncodedString;

- (NSString*)rds_urlDecodedString;

@end
