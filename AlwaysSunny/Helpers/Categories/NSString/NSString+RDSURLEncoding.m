//
//  NSString+RDSURLEncoding.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 29/10/15.
//  Copyright © 2015 Rambler DS. All rights reserved.
//

#import "NSString+RDSURLEncoding.h"

@implementation NSString (RDSURLEncoding)

- (NSString *)rds_urlEncodedString {
    NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
    NSString *result = [self stringByAddingPercentEncodingWithAllowedCharacters:set];
    return result;
}

- (NSString *)rds_urlDecodedString {
    return [self stringByRemovingPercentEncoding];
}

@end