//
//  CommonConstants.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonConstants.h"

#pragma mark - Errors

NSString *const ALSErrorDomain = @"com.serkrapiv.AlwaysSunny.ErrorDomain";
