//
//  APIMethods.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#ifndef APIMethods_h
#define APIMethods_h

extern NSString *const ALSHTTPMethodGET;

extern NSString *const ALSSearchMethodName;
extern NSString *const ALSWeatherMethodName;

#endif /* APIMethods_h */
