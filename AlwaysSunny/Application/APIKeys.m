//
//  APIKeys.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIKeys.h"

#pragma mark - Response

NSString *const ALSDataKey = @"data";
NSString *const ALSCurrentConditionKey = @"current_condition";
NSString *const ALSWeatherKey = @"weather";
NSString *const ALSErrorKey = @"error";
NSString *const ALSErrorMessageKey = @"msg";
NSString *const ALSSearchAPIKey = @"search_api";
NSString *const ALSResultKey = @"result";
NSString *const ALSResponseKey = @"response";

#pragma mark - Request

NSString *const ALSWeatherAPIKeyKey = @"key";
NSString *const ALSSearchQueryKey = @"query";
NSString *const ALSWeatherQueryKey = @"q";
NSString *const ALSFormatKey = @"format";
NSString *const ALSNumberOfDaysKey = @"num_of_days";