//
//  CommonConstants.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#ifndef CommonConstants_h
#define CommonConstants_h

#pragma mark - Errors

extern NSString *const ALSErrorDomain;

#endif /* CommonConstants_h */