//
//  APIMethods.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIMethods.h"

NSString *const ALSHTTPMethodGET = @"GET";

NSString *const ALSSearchMethodName = @"search.ashx";
NSString *const ALSWeatherMethodName = @"weather.ashx";