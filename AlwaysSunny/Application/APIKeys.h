//
//  APIKeys.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#ifndef APIKeys_h
#define APIKeys_h

#pragma mark - Response 

extern NSString *const ALSDataKey;
extern NSString *const ALSCurrentConditionKey;
extern NSString *const ALSWeatherKey;
extern NSString *const ALSErrorKey;
extern NSString *const ALSErrorMessageKey;
extern NSString *const ALSSearchAPIKey;
extern NSString *const ALSResultKey;
extern NSString *const ALSResponseKey;

#pragma mark - Request

extern NSString *const ALSWeatherAPIKeyKey;
extern NSString *const ALSSearchQueryKey;
extern NSString *const ALSWeatherQueryKey;
extern NSString *const ALSFormatKey;
extern NSString *const ALSNumberOfDaysKey;

#endif /* APIKeys_h */
