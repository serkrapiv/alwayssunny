//
//  RequestSigner.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @author Sergey Krapivenskiy, 16-03-17 21:03:05
 
 Used to sign NSURLRequests with some additional data, like API keys
 */
@protocol RequestSigner <NSObject>

/**
 @author Sergey Krapivenskiy, 16-03-17 21:03:27
 
 Signs preconfigured NSURLRequest
 
 @param request Request
 
 @return Signed request
 */
- (NSURLRequest *)signRequest:(NSURLRequest *)request;

@end
