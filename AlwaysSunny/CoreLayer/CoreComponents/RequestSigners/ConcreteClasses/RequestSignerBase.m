//
//  RequestSignerBase.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "RequestSignerBase.h"
#import "APIKeys.h"

@implementation RequestSignerBase

- (NSURLRequest *)signRequest:(NSURLRequest *)request {
    NSMutableURLRequest *signedRequest = [request mutableCopy];
    NSURL *requestURL = signedRequest.URL;
    
    NSString *queryString = requestURL.query;
    
    NSString *keyString = [NSString stringWithFormat:@"%@=%@", ALSWeatherAPIKeyKey, self.APIKey];
    
    NSMutableString *absoluteURLString = [requestURL.absoluteString mutableCopy];
    if (queryString.length != 0) {
        [absoluteURLString appendString:@"&"];
    }
    [absoluteURLString appendString:keyString];
    signedRequest.URL = [NSURL URLWithString:absoluteURLString];
    
    return signedRequest;
}

@end