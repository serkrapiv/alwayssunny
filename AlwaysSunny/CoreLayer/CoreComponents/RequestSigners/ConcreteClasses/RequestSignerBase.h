//
//  RequestSignerBase.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestSigner.h"

extern NSString *const ALSKeyParam;

@interface RequestSignerBase : NSObject <RequestSigner>

@property (nonatomic, copy) NSString *APIKey;

@end
