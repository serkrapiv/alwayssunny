//
//  URLSessionNetworkClient.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 16/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkClient.h"

/**
 @author Sergey Krapivenskiy, 16-03-16 23:03:36
 
 Network client that uses NSURLSession to send requests
 */
@interface URLSessionNetworkClient : NSObject <NetworkClient>

@property (nonatomic, strong) NSURLSession *session;

@end
