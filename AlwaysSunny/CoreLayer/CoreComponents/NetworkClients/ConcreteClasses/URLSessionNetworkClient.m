//
//  URLSessionNetworkClient.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 16/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "URLSessionNetworkClient.h"

@implementation URLSessionNetworkClient

#pragma mark - NetworkClient

- (void)performRequest:(nonnull NSURLRequest*)request
        withCompletion:(nullable NetworkClientCompletionBlock)completionBlock {
    NSAssert(request != nil, @"NSURLRequest cannot be nil");
    
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (completionBlock) {
            completionBlock(data, error);
        }
    }];
    
    [dataTask resume];
}

@end