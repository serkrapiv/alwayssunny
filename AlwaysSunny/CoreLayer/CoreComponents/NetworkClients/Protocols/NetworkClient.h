//
//  NetworkClient.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 16/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NetworkClientCompletionBlock)(_Nullable id data,  NSError * _Nullable error);

/**
 @author Sergey Krapivenskiy, 16-03-16 23:03:03
 
 Base interface for API requests
 */
@protocol NetworkClient <NSObject>

/**
 @author Sergey Krapivenskiy, 16-03-16 23:03:43
 
 Sends pre-configured request
 
 @param request         Pre-configured NSURLRequest
 @param completionBlock Result block
 */
- (void)performRequest:(nonnull NSURLRequest*)request
        withCompletion:(nullable NetworkClientCompletionBlock)completionBlock;

@end
