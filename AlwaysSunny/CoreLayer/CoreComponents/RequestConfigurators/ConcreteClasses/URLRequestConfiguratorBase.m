//
//  URLRequestConfiguratorBase.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "URLRequestConfiguratorBase.h"

#import "RequestModel.h"
#import "NSMutableArray+RDSNilFilter.h"
#import "NSArray+RDSNonEmptyStrings.h"
#import "NSDictionary+RDSHTTPQueryParameters.h"

@interface URLRequestConfiguratorBase ()

@property (nonatomic, copy) NSURL *baseURL;

@end

@implementation URLRequestConfiguratorBase

- (instancetype)initWithBaseURL:(NSURL *)baseURL {
    if (self = [super init]) {
        _baseURL = [baseURL copy];
    }
    return self;
}

#pragma mark - URLRequestConfiguratorProtocol

- (NSURLRequest *)requestWithMethod:(NSString *)method
                           urlParts:(NSArray *)urlParts
                       requestModel:(RequestModel *)requestModel {
    
    NSDictionary *queryParameters = requestModel.queryParameters;
    NSURL *requestURL = [self URLWithParts:urlParts
                                parameters:queryParameters];
    
    NSMutableURLRequest *mutableRequest = [NSMutableURLRequest requestWithURL:requestURL];
    mutableRequest.HTTPMethod = method;
    
    NSDictionary *headerFields = requestModel.httpHeaderFields;
    if (headerFields) {
        for (NSString *headerField in [headerFields allKeys]) {
            NSString *value = headerFields[headerField];
            [mutableRequest setValue:value
                  forHTTPHeaderField:headerField];
        }
    }
    return [mutableRequest copy];
}

#pragma mark - Helper

- (NSURL *)URLWithParts:(NSArray*)otherUrlParts
             parameters:(NSDictionary*)parameters {
    
    NSMutableArray *urlParts = [NSMutableArray array];
    
    [urlParts rds_addString:self.baseURL.absoluteString];
    [urlParts rds_addString:[otherUrlParts rds_nonemptyComponentsJoinedByString:@"/"]];
    [urlParts rds_addString:[parameters rds_queryParametersStringEscaped:YES]];
    
    NSString *urlPath = [urlParts rds_nonemptyComponentsJoinedByString:@"/"];
    urlPath = [urlPath stringByReplacingOccurrencesOfString:@"/?"
                                                 withString:@"?"];
    
    NSRange rangeWithoutBaseURL = NSMakeRange(self.baseURL.absoluteString.length, urlPath.length - self.baseURL.absoluteString.length);
    
    urlPath = [urlPath stringByReplacingOccurrencesOfString:@"//"
                                                 withString:@"/"
                                                    options:0
                                                      range:rangeWithoutBaseURL];
    
    return [NSURL URLWithString: urlPath];
}

@end
