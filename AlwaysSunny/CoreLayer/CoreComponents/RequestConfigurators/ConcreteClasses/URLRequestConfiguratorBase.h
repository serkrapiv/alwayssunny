//
//  URLRequestConfiguratorBase.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "URLRequestConfigurator.h"

@interface URLRequestConfiguratorBase : NSObject <URLRequestConfigurator>

- (instancetype)initWithBaseURL:(NSURL *)baseURL;

@end