//
//  RequestModel.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:41
 
 Contains parameters, used to create NSURLRequest
 */
@interface RequestModel : NSObject

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:04
 
 Returns empty model
 
 @return RequestModel
 */
+ (instancetype)requestModel;

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:34
 
 HTTP header fields
 */
@property (copy, nonatomic) NSDictionary *httpHeaderFields;

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:52
 
 Parameters for query requests
 */
@property (copy, nonatomic) NSDictionary *queryParameters;

@end