//
//  RequestModel.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "RequestModel.h"

@implementation RequestModel

+ (instancetype)requestModel {
    return [[[self class] alloc] init];
}

@end
