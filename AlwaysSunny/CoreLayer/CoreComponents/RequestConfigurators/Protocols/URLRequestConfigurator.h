//
//  URLRequestConfigurator.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RequestModel;

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:01
 
 Describes object that configures NSURLRequest with specified parameters
 */
@protocol URLRequestConfigurator <NSObject>

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:48
 
 Generic method to create and configure an NSURLRequest
 
 @param method       An NSString with method's name. Constants from APIMethods.h are used
 @param urlParts     An array with constant or variable URL parts. For example, for an URL like %BASE_URL%/comment/:commentId this array will contain two objects: "comment" string and comment identifier
 @param requestModel Request data model. Contains HTTP headers, query and body parameters
 
 @return NSURLRequest
 */
- (NSURLRequest *)requestWithMethod:(NSString *)method
                           urlParts:(NSArray *)urlParts
                       requestModel:(RequestModel *)requestModel;

@end