//
//  ResponseValidator.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:03
 
 Validates deserialized server responses. Checks that response structure is valid and does not contain errors
 */
@protocol ResponseValidator <NSObject>

- (NSError * _Nullable )validateServerResponse:(_Nullable id)response;

@end
