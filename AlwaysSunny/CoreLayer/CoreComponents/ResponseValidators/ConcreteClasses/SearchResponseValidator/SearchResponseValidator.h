//
//  SearchResponseValidator.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "ResponseValidatorBase.h"
#import "ResponseValidator.h"

@interface SearchResponseValidator : ResponseValidatorBase <ResponseValidator>

@end
