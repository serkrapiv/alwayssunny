//
//  SearchResponseValidator.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "SearchResponseValidator.h"
#import "APIKeys.h"

@implementation SearchResponseValidator

#pragma mark - ResponseValidator

- (NSError *)validateServerResponse:(id)response {
    NSError *validationError = nil;
    
    [self validateResponseIsDictionary:response
                                 error:&validationError];
    
    if (validationError) {
        return validationError;
    }
    
    if (response[ALSSearchAPIKey] == nil) {
        validationError = [self errorForResponse:response];
        return validationError;
    }
    
    NSDictionary *dataDictionary = response[ALSSearchAPIKey];
    
    if (dataDictionary[ALSErrorKey] != nil) {
        validationError = [self errorForResponse:response];
        return validationError;
    }
    
    if (dataDictionary[ALSResultKey] == nil) {
        validationError = [self errorForResponse:response];
        return validationError;
    }
    
    return nil;
}

#pragma mark - Helpers

- (NSError *)errorForResponse:(id)response {
    id responseOrNil = (response == nil ? [NSNull null] : response);
    NSDictionary *userData = @{ ALSResponseKey : responseOrNil };
    
    NSError *error = [NSError errorWithDomain:ALSDataKey
                                         code:0
                                     userInfo:userData];
    
    return error;
}

@end
