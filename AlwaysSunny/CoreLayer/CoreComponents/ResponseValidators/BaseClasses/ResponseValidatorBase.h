//
//  ResponseValidatorBase.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResponseValidatorBase : NSObject

/**
 @author Sergey Krapivenskiy
 
 Validates that the provided structure is a dictionary
 
 @param response Deserialized server response
 @param error    NSError
 
 @return The result of the validation process
 */
- (BOOL)validateResponseIsDictionary:(id)response error:(NSError **)error;

@end
