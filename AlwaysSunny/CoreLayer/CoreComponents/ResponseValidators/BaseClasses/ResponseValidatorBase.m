//
//  ResponseValidatorBase.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "ResponseValidatorBase.h"

static NSString *const ALSResponseValidationErrorDomain = @"com.serkrapiv.AlwaysSunny.validation-error-domain";

static NSUInteger const ALSInvalidResponseErrorCode = 1;

@implementation ResponseValidatorBase

- (BOOL)validateResponseIsDictionary:(id)response
                               error:(NSError *__autoreleasing *)error {
    if (![response isKindOfClass: [NSDictionary class]]) {
        NSDictionary *userData = @{
                                   @"response" : (response ?: [NSNull null])
                                   };
        
        *error = [NSError errorWithDomain:ALSResponseValidationErrorDomain
                                     code:ALSInvalidResponseErrorCode
                                 userInfo:userData];
        return NO;
    }
    return YES;
}

@end
