//
//  JSONResponseDeserializer.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "JSONResponseDeserializer.h"

@implementation JSONResponseDeserializer

- (void)deserializeServerResponse:(NSData *)responseData
                  completionBlock:(DeserializerCompletionBlock)completionBlock {
    
    NSError *serializationError = nil;
    id responseObject = nil;
    
    if (responseData == nil) {
        responseObject = nil;
    } else {
        responseObject = [NSJSONSerialization JSONObjectWithData:responseData
                                                         options:0
                                                           error:&serializationError];
    }
    
    completionBlock(responseObject, serializationError);
}

@end
