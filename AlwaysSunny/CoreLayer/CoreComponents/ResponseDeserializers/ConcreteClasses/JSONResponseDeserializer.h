//
//  JSONResponseDeserializer.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponseDeserializer.h"

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:18
 
 A simple deserializer of JSON responses
 */
@interface JSONResponseDeserializer : NSObject <ResponseDeserializer>

@end
