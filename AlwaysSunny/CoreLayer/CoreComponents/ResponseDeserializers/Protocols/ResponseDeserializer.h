//
//  ResponseDeserializer.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^DeserializerCompletionBlock)(NSDictionary  * _Nullable response, NSError  * _Nullable error);

/**
 @author Sergey Krapivenskiy, 16-03-17 00:03:31
 
 Deserializes server responses
 */
@protocol ResponseDeserializer <NSObject>

- (void)deserializeServerResponse:(nonnull NSData *)responseData
                  completionBlock:(nullable DeserializerCompletionBlock)completionBlock;

@end