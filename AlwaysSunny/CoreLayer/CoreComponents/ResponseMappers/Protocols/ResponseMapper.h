//
//  ResponseMapper.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MappingContext;

/**
 @author Sergey Krapivenskiy, 16-03-17 14:03:45
 
 Describes objects responsible for transforming source objects (most likely server responses) to model objects
 */
@protocol ResponseMapper <NSObject>

/**
 @author Sergey Krapivenskiy, 16-03-17 14:03:28
 
 Maps source object
 
 @param sourceObject Source object, i.e. server response
 @param context      Model object with additional info about mapping
 @param error        Mapping error
 
 @return Model object
 */
- (id)mapSourceObject:(id)sourceObject
   withMappingContext:(MappingContext *)context
                error:(NSError **)error;

@end
