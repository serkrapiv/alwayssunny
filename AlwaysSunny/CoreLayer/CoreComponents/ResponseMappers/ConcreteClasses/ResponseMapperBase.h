//
//  ResponseMapperBase.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponseMapper.h"

@protocol MappingProvider;

@interface ResponseMapperBase : NSObject <ResponseMapper>

@property (nonatomic, strong) id<MappingProvider> mappingProvider;

@end
