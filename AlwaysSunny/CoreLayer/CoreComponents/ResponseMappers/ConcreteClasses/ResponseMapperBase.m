//
//  ResponseMapperBase.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "ResponseMapperBase.h"
#import <EasyMapping/EasyMapping.h>

#import "MappingContext.h"
#import "MappingProvider.h"

#import "CommonConstants.h"

static NSString *const ALSMappingNotFoundErrorDescription = @"Mapping not found";

@implementation ResponseMapperBase

#pragma mark - ResponseMapper

- (id)mapSourceObject:(id)sourceObject
   withMappingContext:(MappingContext *)context
                error:(NSError *__autoreleasing *)error {
    
    EKObjectMapping *mapping = [self.mappingProvider mappingForType:context.mappingType];
    
    if (!mapping) {
        *error = [NSError errorWithDomain:ALSErrorDomain
                                     code:0
                                 userInfo:@{ NSLocalizedDescriptionKey : ALSMappingNotFoundErrorDescription }];
        
        return nil;
    }
    
    id mappedObject = context.keyPath ? [sourceObject valueForKeyPath:context.keyPath] : sourceObject;
    
    if ([mappedObject isKindOfClass:[NSDictionary class]]) {
        id mappingResult = [EKMapper objectFromExternalRepresentation:mappedObject
                                                          withMapping:mapping];
        return mappingResult;
        
    } else if ([mappedObject isKindOfClass:[NSArray class]]) {
        id mappingResult = [EKMapper arrayOfObjectsFromExternalRepresentation:mappedObject
                                                                  withMapping:mapping];
        return mappingResult;
    }
    
    return nil;
}

@end
