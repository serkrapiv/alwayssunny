//
//  MappingContext.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MappingType.h"

/**
 @author Sergey Krapivenskiy, 16-03-17 14:03:43
 
 Contains information that mapping provider uses to select required mapping
 */
@interface MappingContext : NSObject

@property (nonatomic, assign) ALSMappingType mappingType;
@property (nonatomic, copy) NSString *keyPath;

@end
