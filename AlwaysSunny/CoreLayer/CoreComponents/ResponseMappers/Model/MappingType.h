//
//  MappingType.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#ifndef MappingType_h
#define MappingType_h

/**
 @author Sergey Krapivenskiy, 16-03-17 14:03:57
 
 This enum is used to receive mapping of specific type from mapping provider
 */
typedef NS_ENUM(NSUInteger, ALSMappingType) {
    /**
     @author Sergey Krapivenskiy, 16-03-17 14:03:57
     
     Search result mapping type
     */
    ALSMappingTypeSearchResult = 0,
    /**
     @author Sergey Krapivenskiy, 16-03-17 14:03:57
     
     Weather forecast mapping type
     */
    ALSMappingTypeWeather = 1
};

#endif /* MappingType_h */
