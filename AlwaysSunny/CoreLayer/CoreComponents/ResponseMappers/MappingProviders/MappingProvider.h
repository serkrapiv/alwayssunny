//
//  MappingProvider.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MappingType.h"

@protocol MappingProvider <NSObject>

- (id)mappingForType:(ALSMappingType)mappingType;

@end
