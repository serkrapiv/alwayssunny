//
//  MappingProviderBase.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MappingProvider.h"

@interface MappingProviderBase : NSObject <MappingProvider>

@end
