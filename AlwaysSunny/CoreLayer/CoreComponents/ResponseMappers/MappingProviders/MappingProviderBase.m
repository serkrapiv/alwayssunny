//
//  MappingProviderBase.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "MappingProviderBase.h"
#import <EasyMapping/EasyMapping.h>

#import "Astronomy.h"
#import "DailyForecast.h"
#import "City.h"

@interface MappingProviderBase ()

@property (nonatomic, strong) NSDictionary *mappingsDictionary;

@end

@implementation MappingProviderBase

#pragma mark - MappingProvider

- (id)mappingForType:(ALSMappingType)mappingType {
    return self.mappingsDictionary[@(mappingType)];
}

#pragma mark - Mappings dictionary

- (NSDictionary *)mappingsDictionary {
    if (!_mappingsDictionary) {
        _mappingsDictionary = @{ @(ALSMappingTypeWeather) : [self weatherMapping],
                                 @(ALSMappingTypeSearchResult) : [self cityMapping]};
    }
    return _mappingsDictionary;
}

#pragma mark - Mappings

- (EKObjectMapping *)cityMapping {
    
    EKObjectMapping *mapping = [EKObjectMapping mappingForClass:[City class] withBlock:^(EKObjectMapping *mapping) {
        
        [mapping mapPropertiesFromArray:@[ NSStringFromSelector(@selector(latitude)),
                                           NSStringFromSelector(@selector(longitude)),
                                           NSStringFromSelector(@selector(population)) ]];
        
        [mapping mapKeyPath:@"areaName.value" toProperty:NSStringFromSelector(@selector(name)) withValueBlock:^id(NSString *key, NSArray *value) {
            return value.firstObject;
        }];
        
        [mapping mapKeyPath:@"country.value" toProperty:NSStringFromSelector(@selector(country)) withValueBlock:^id(NSString *key, NSArray *value) {
            return value.firstObject;
        }];
        
        [mapping mapKeyPath:@"region.value" toProperty:NSStringFromSelector(@selector(region)) withValueBlock:^id(NSString *key, NSArray *value) {
            return value.firstObject;
        }];
        
        [mapping mapKeyPath:@"weatherUrl.value" toProperty:NSStringFromSelector(@selector(weatherURL)) withValueBlock:^id(NSString *key, NSArray *value) {
            NSString *URLAsString = value.firstObject;
            
            return [NSURL URLWithString:URLAsString];;
        }];
    }];
    
    return mapping;
}

- (EKObjectMapping *)weatherMapping {
    NSDictionary *mappingsDictionary =
    @{ @"maxtempC" : NSStringFromSelector(@selector(maxTemperatureCelsius)),
       @"maxtempF" : NSStringFromSelector(@selector(maxTemperatureFahrenheit)),
       @"mintempC" : NSStringFromSelector(@selector(minTemperatureCelsius)),
       @"mintempF" : NSStringFromSelector(@selector(minTemperatureFahrenheit)),
       @"uvIndex" : NSStringFromSelector(@selector(uvIndex)) };
    
    __weak typeof(self) wSelf = self;
    EKObjectMapping *mapping = [EKObjectMapping mappingForClass:[DailyForecast class]withBlock:^(EKObjectMapping *mapping) {
        
        [mapping mapPropertiesFromDictionary:mappingsDictionary];
        
        [mapping mapKeyPath:@"date"
                 toProperty:NSStringFromSelector(@selector(dateString))];
        
        [mapping hasMany:[Astronomy class]
              forKeyPath:NSStringFromSelector(@selector(astronomy))
             forProperty:NSStringFromSelector(@selector(astronomyObjects))
       withObjectMapping:[wSelf astronomyMapping]];
    }];
    
    return mapping;
}

- (EKObjectMapping *)astronomyMapping {
    
    NSDictionary *mappingsDictionary =
    @{ @"sunrise" : NSStringFromSelector(@selector(sunriseTime)),
       @"sunset" : NSStringFromSelector(@selector(sunsetTime)),
       @"moonrise" : NSStringFromSelector(@selector(moonriseTime)),
       @"moonset" : NSStringFromSelector(@selector(moonsetTime)) };
    
    EKObjectMapping *mapping = [EKObjectMapping mappingForClass:[Astronomy class]
                                                      withBlock:^(EKObjectMapping *mapping)
                                {
                                    [mapping mapPropertiesFromDictionary:mappingsDictionary];
                                }];
    
    return mapping;
}

@end