//
//  CoreLayerAssembly.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "CoreLayerAssembly.h"

#import "URLSessionNetworkClient.h"
#import "URLRequestConfiguratorBase.h"
#import "JSONResponseDeserializer.h"
#import "ResponseValidatorBase.h"
#import "SearchResponseValidator.h"
#import "WeatherResponseValidator.h"
#import "ResponseMapperBase.h"
#import "MappingProviderBase.h"
#import "RequestSignerBase.h"

static NSString *const ALSCoreLayerConfigFileName = @"CoreLayerConfig.plist";
static NSString *const ALSBaseAPIURLKey = @"BaseAPIURL";
static NSString *const ALSAPIKeyKey = @"APIKey";

@implementation CoreLayerAssembly

- (id<NetworkClient>)networkClient {
    return [TyphoonDefinition withClass:[URLSessionNetworkClient class]
                          configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(session)
                              with:[NSURLSession sharedSession]];
    }];
}

- (id<RequestConfigurator>)requestConfigurator {
    return [TyphoonDefinition withClass:[URLRequestConfiguratorBase class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition useInitializer:@selector(initWithBaseURL:)
                                parameters:^(TyphoonMethod *initializer)
                 {
                     [initializer injectParameterWith:TyphoonConfig(ALSBaseAPIURLKey)];
                 }];
            }];
}

- (id<RequestSigner>)requestSigner {
    return [TyphoonDefinition withClass:[RequestSignerBase class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(APIKey)
                                      with:TyphoonConfig(ALSAPIKeyKey)];
            }];
}

- (id<ResponseDeserializer>)responseDeserializer {
    return [TyphoonDefinition withClass:[JSONResponseDeserializer class]];
}

- (id<ResponseValidator>)searchResponseValidator {
    return [TyphoonDefinition withClass:[SearchResponseValidator class]];
}

- (id<ResponseValidator>)weatherResponseValidator {
    return [TyphoonDefinition withClass:[WeatherResponseValidator class]];
}

- (id<ResponseMapper>)responseMapper {
    return [TyphoonDefinition withClass:[ResponseMapperBase class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(mappingProvider)
                                      with:[self mappingProvider]];
            }];
}

- (id<MappingProvider>)mappingProvider {
    return [TyphoonDefinition withClass:[MappingProviderBase class]];
}

#pragma mark - Config

- (id)configurer {
    NSBundle *currentBundle = [NSBundle bundleForClass:[self class]];
    return [TyphoonDefinition withConfigName:ALSCoreLayerConfigFileName
                                      bundle:currentBundle];
}

@end