//
//  CoreLayerAssembly.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@protocol NetworkClient;
@protocol RequestConfigurator;
@protocol ResponseDeserializer;
@protocol ResponseValidator;
@protocol ResponseMapper;
@protocol RequestSigner;

@interface CoreLayerAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (id<NetworkClient>)networkClient;
- (id<RequestConfigurator>)requestConfigurator;
- (id<RequestSigner>)requestSigner;
- (id<ResponseDeserializer>)responseDeserializer;
- (id<ResponseValidator>)searchResponseValidator;
- (id<ResponseValidator>)weatherResponseValidator;
- (id<ResponseMapper>)responseMapper;

@end