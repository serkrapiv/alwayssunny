//
//  RequestManagerBase.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "RequestManagerBase.h"

#import "URLRequestConfigurator.h"
#import "NetworkClient.h"
#import "ResponseDeserializer.h"
#import "ResponseValidator.h"
#import "ResponseMapper.h"
#import "RequestSigner.h"

#import "RequestModel.h"
#import "MappingContext.h"

@implementation RequestManagerBase

- (void)performRequestWithMethod:(NSString *)method
                        urlParts:(NSArray *)urlParts
                    requestModel:(RequestModel *)requestModel
                  mappingContext:(MappingContext *)mappingContext
                      completion:(ServiceCompletionBlockWithArrayOfObjects)completion {
    
    NSURLRequest *request = [self.requestConfigurator requestWithMethod:method
                                                               urlParts:urlParts
                                                           requestModel:requestModel];
    
    __weak typeof(self) wSelf = self;
    
    if (self.requestSigner) {
        request = [self.requestSigner signRequest:request];
    }
    
    [self.networkClient performRequest:request
                        withCompletion:^(id data, NSError *error)
     {
         if (error) {
             completion(nil, error);
             return;
         }
         
         [wSelf deserializeServerResponse:data
                       withMappingContext:mappingContext
                          completionBlock:completion];
         
     }];
    
}

#pragma mark - Private

- (void)deserializeServerResponse:(id)response
               withMappingContext:(MappingContext *)mappingContext
                  completionBlock:(ServiceCompletionBlockWithArrayOfObjects)completion {
    
    __weak typeof(self) wSelf = self;
    
    [wSelf.responseDeserializer deserializeServerResponse:response
                                          completionBlock:^(NSDictionary *response, NSError *error)
     {
         if (error) {
             completion(nil, error);
             return;
         }
         
         NSError *validationError = [wSelf.responseValidator validateServerResponse:response];
         
         if (validationError) {
             completion(nil, validationError);
             return;
         }
         
         NSError *mappingError = nil;
         
         id mappingResult = [wSelf.responseMapper mapSourceObject:response
                                               withMappingContext:mappingContext
                                                            error:&mappingError];
         
         completion(mappingResult, mappingError);
     }];
}

@end
