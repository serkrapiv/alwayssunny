//
//  RequestManager.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceCompletionBlocks.h"

@protocol URLRequestConfigurator;
@protocol NetworkClient;
@protocol ResponseDeserializer;
@protocol ResponseValidator;
@protocol ResponseMapper;
@protocol RequestSigner;

@class RequestModel;
@class MappingContext;

@protocol RequestManager <NSObject>

- (void)performRequestWithMethod:(NSString *)method
                        urlParts:(NSArray *)urlParts
                    requestModel:(RequestModel *)requestModel
                  mappingContext:(MappingContext *)mappingContext
                      completion:(ServiceCompletionBlockWithArrayOfObjects)completion;

- (id<URLRequestConfigurator>)requestConfigurator;
- (id<NetworkClient>)networkClient;
- (id<RequestSigner>)requestSigner;
- (id<ResponseDeserializer>)responseDeserializer;
- (id<ResponseValidator>)responseValidator;
- (id<ResponseMapper>)responseMapper;

@end
