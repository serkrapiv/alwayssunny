//
//  RequestManagerBase.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestManager.h"

@interface RequestManagerBase : NSObject <RequestManager>

@property (nonatomic, strong) id<URLRequestConfigurator> requestConfigurator;
@property (nonatomic, strong) id<NetworkClient> networkClient;
@property (nonatomic, strong) id<RequestSigner> requestSigner;
@property (nonatomic, strong) id<ResponseDeserializer> responseDeserializer;
@property (nonatomic, strong) id<ResponseValidator> responseValidator;
@property (nonatomic, strong) id<ResponseMapper> responseMapper;

@end
