//
//  City.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "City.h"

@implementation City

#pragma mark - <NSCoding>

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.name
                  forKey:NSStringFromSelector(@selector(name))];
    [aCoder encodeObject:self.country
                  forKey:NSStringFromSelector(@selector(country))];
    [aCoder encodeObject:self.region
                  forKey:NSStringFromSelector(@selector(region))];
    [aCoder encodeObject:self.weatherURL
                  forKey:NSStringFromSelector(@selector(weatherURL))];
    [aCoder encodeDouble:self.latitude
                  forKey:NSStringFromSelector(@selector(latitude))];
    [aCoder encodeDouble:self.longitude
                  forKey:NSStringFromSelector(@selector(longitude))];
    [aCoder encodeInteger:self.population
                  forKey:NSStringFromSelector(@selector(population))];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _name = [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(name))];
        _country = [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(country))];
        _region = [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(region))];
        _weatherURL = [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(weatherURL))];
        _latitude = [aDecoder decodeDoubleForKey:NSStringFromSelector(@selector(latitude))];
        _longitude = [aDecoder decodeDoubleForKey:NSStringFromSelector(@selector(longitude))];
        _population = [aDecoder decodeIntegerForKey:NSStringFromSelector(@selector(population))];
    }
    return self;
}

#pragma mark - Equality

- (BOOL)isEqualToCity:(City *)city {
    if (city == nil) {
        return NO;
    }
    
    return [self.name isEqualToString:city.name] && ([self locationData]) == ([city locationData]);
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    return [self isEqualToCity:object];
}

- (NSUInteger)hash {
    NSString *className = NSStringFromClass([self class]);
    return [className hash] ^ super.hash ^ self.name.hash;
}

#pragma mark - Helpers

- (NSUInteger)locationData {
    NSUInteger offset = 1000;
    NSUInteger precision = 100;
    NSUInteger location = ((NSUInteger)(self.latitude * precision) * offset) + (NSUInteger)(self.longitude * precision);
    return location;
}

@end