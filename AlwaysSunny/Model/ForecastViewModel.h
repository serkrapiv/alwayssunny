//
//  ForecastViewModel.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ForecastViewModel : NSObject

@property (nonatomic, copy) NSString *dateString;
@property (nonatomic, copy) NSString *minTemperature;
@property (nonatomic, copy) NSString *maxTemperature;
@property (nonatomic, copy) NSString *sunrise;
@property (nonatomic, copy) NSString *sunset;

@end