//
//  DailyForecast.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Astronomy.h"

@interface DailyForecast : NSObject

@property (nonatomic, copy) NSString *dateString;
@property (nonatomic, assign) NSInteger maxTemperatureCelsius;
@property (nonatomic, assign) NSInteger maxTemperatureFahrenheit;
@property (nonatomic, assign) NSInteger minTemperatureCelsius;
@property (nonatomic, assign) NSInteger minTemperatureFahrenheit;
@property (nonatomic, assign) NSInteger uvIndex;
@property (nonatomic, strong) NSArray *astronomyObjects;
@property (nonatomic, strong, readonly) Astronomy *astronomy;

@end
