//
//  DailyForecast.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "DailyForecast.h"

@implementation DailyForecast

/**
 @author Sergey Krapivenskiy, 16-03-18 13:03:29
 
 Normally I don't work with EasyMapping, so I didn't figure out in time how to map an array of single object into an object. That's why we have this workaround here
*/
- (Astronomy *)astronomy {
    return self.astronomyObjects.firstObject;
}

@end
