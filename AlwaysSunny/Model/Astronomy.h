//
//  Astronomy.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Astronomy : NSObject

@property (nonatomic, copy) NSString *sunriseTime;
@property (nonatomic, copy) NSString *sunsetTime;
@property (nonatomic, copy) NSString *moonriseTime;
@property (nonatomic, copy) NSString *moonsetTime;

@end
