//
//  CitiesListInteractor.m
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitiesListInteractor.h"

#import "CitiesListInteractorOutput.h"

#import "CitiesService.h"

@implementation CitiesListInteractor

#pragma mark - CitiesListInteractorInput

- (NSArray *)obtainSavedCities {
    return [self.citiesService obtainSavedCities];
}

- (void)removeCity:(City *)city {
    [self.citiesService removeCity:city];
}

@end