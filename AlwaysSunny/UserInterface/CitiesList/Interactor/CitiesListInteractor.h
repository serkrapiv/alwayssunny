//
//  CitiesListInteractor.h
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitiesListInteractorInput.h"

@protocol CitiesListInteractorOutput;
@protocol CitiesService;

@interface CitiesListInteractor : NSObject <CitiesListInteractorInput>

@property (nonatomic, weak) id<CitiesListInteractorOutput> output;

@property (nonatomic, strong) id<CitiesService> citiesService;

@end