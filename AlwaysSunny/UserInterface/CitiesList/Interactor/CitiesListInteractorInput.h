//
//  CitiesListInteractorInput.h
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceCompletionBlocks.h"

@class City;

@protocol CitiesListInteractorInput <NSObject>

- (NSArray *)obtainSavedCities;

- (void)removeCity:(City *)city;

@end