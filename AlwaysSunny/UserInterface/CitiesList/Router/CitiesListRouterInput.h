//
//  CitiesListRouterInput.h
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@class City;

@protocol CitySearchModuleOutput;

@protocol CitiesListRouterInput

- (void)openCitySearchModuleWithOutput:(id<CitySearchModuleOutput>)moduleOutput;

- (void)openForecastModuleForCity:(City *)city;

@end