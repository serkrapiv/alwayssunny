//
//  CitiesListRouter.h
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitiesListRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface CitiesListRouter : NSObject <CitiesListRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end