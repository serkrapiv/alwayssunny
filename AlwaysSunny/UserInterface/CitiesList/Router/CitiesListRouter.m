//
//  CitiesListRouter.m
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitiesListRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "CitySearchModuleInput.h"
#import "CitySearchModuleOutput.h"

#import "ForecastModuleInput.h"

#import "City.h"

static NSString *const ALSShowForecastsSegue = @"ShowForecastsSegue";
static NSString *const ALSShowCitySearchSegue = @"ShowCitySearchSegue";

@implementation CitiesListRouter

#pragma mark - CitiesListRouterInput

- (void)openCitySearchModuleWithOutput:(id<CitySearchModuleOutput>)moduleOutput {
    [[self.transitionHandler openModuleUsingSegue:ALSShowCitySearchSegue] thenChainUsingBlock:^id<CitySearchModuleOutput>(id<CitySearchModuleInput> moduleInput) {
        [moduleInput configureModuleWithOutput:moduleOutput];
        return moduleOutput;
    }];
}

- (void)openForecastModuleForCity:(City *)city {
    [[self.transitionHandler openModuleUsingSegue:ALSShowForecastsSegue] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<ForecastModuleInput> moduleInput) {
        [moduleInput configureModuleWithCityName:city.name];
        return nil;
    }];
}

@end