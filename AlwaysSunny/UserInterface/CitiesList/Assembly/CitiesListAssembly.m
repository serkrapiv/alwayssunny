//
//  CitiesListAssembly.m
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitiesListAssembly.h"

#import "ServicesAssembly.h"

#import "CitiesListViewController.h"
#import "CitiesListInteractor.h"
#import "CitiesListPresenter.h"
#import "CitiesListRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@interface CitiesListAssembly ()

@property (nonatomic, strong) ServicesAssembly *servicesAssembly;

@end

@implementation CitiesListAssembly

- (CitiesListViewController *)viewCitiesListModule {
    return [TyphoonDefinition withClass:[CitiesListViewController class]
                          configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCitiesListModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterCitiesListModule]];
                          }];
}

- (CitiesListInteractor *)interactorCitiesListModule {
    return [TyphoonDefinition withClass:[CitiesListInteractor class]
                          configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCitiesListModule]];
                              [definition injectProperty:@selector(citiesService) with:[self.servicesAssembly citiesService]];
                          }];
}

- (CitiesListPresenter *)presenterCitiesListModule {
    return [TyphoonDefinition withClass:[CitiesListPresenter class]
                          configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCitiesListModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCitiesListModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCitiesListModule]];
                          }];
}

- (CitiesListRouter *)routerCitiesListModule {

    return [TyphoonDefinition withClass:[CitiesListRouter class]
                           configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCitiesListModule]];
                          }];
}

@end