//
//  CitiesListPresenter.m
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitiesListPresenter.h"

#import "CitiesListViewInput.h"
#import "CitiesListInteractorInput.h"
#import "CitiesListRouterInput.h"

@implementation CitiesListPresenter

#pragma mark - CitiesListViewOutput

- (void)didLoadView {
    [self reloadCities];
}

- (void)didDeleteCity:(City *)city {
    [self.interactor removeCity:city];
}

- (void)didSelectCity:(City *)city {
    [self.router openForecastModuleForCity:city];
}

- (void)didAskToAddCity {
    [self.router openCitySearchModuleWithOutput:self];
}

#pragma mark - CitySearchModuleOutput

- (void)didSelectCityInSearch:(City *)city {
    [self reloadCities];
}

#pragma mark - Private

- (void)reloadCities {
    NSArray *savedCities = [self.interactor obtainSavedCities];
    [self.view showCities:savedCities];
}

@end