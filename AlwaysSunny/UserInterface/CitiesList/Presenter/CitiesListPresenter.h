//
//  CitiesListPresenter.h
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitiesListViewOutput.h"
#import "CitiesListInteractorOutput.h"
#import "CitySearchModuleOutput.h"

@protocol CitiesListViewInput;
@protocol CitiesListInteractorInput;
@protocol CitiesListRouterInput;

@interface CitiesListPresenter : NSObject <CitiesListViewOutput, CitiesListInteractorOutput, CitySearchModuleOutput>

@property (nonatomic, weak) id<CitiesListViewInput> view;
@property (nonatomic, strong) id<CitiesListInteractorInput> interactor;
@property (nonatomic, strong) id<CitiesListRouterInput> router;

@end