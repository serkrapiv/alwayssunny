//
//  CitiesListViewController.m
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitiesListViewController.h"

#import "CitiesListViewOutput.h"

#import "City.h"
#import "CityTableViewCell.h"

@interface CitiesListViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *cities;

@end

@implementation CitiesListViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    NSString *cellClassName = NSStringFromClass([CityTableViewCell class]);
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    UINib *nib = [UINib nibWithNibName:cellClassName bundle:bundle];
    [self.tableView registerNib:nib forCellReuseIdentifier:cellClassName];
    
    self.cities = [NSMutableArray new];
    
	[self.output didLoadView];
}

#pragma mark - Private 

- (void)updateUserInterfaceForCitiesCount:(NSInteger)count {
    self.noCitiesFoundView.hidden = count > 0;
}

#pragma mark - CitiesListViewInput

- (void)showCities:(NSArray *)cities {
    self.cities = [NSMutableArray arrayWithArray:cities];
    [self updateUserInterfaceForCitiesCount:cities.count];
    [self.tableView reloadData];
}
#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cities.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        City *city = self.cities[indexPath.row];
        [self.cities removeObjectAtIndex:indexPath.row];
        [self.output didDeleteCity:city];
        [self updateUserInterfaceForCitiesCount:self.cities.count];
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[ indexPath ]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = NSStringFromClass([CityTableViewCell class]);
    CityTableViewCell *cell = (CityTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    City *city = self.cities[indexPath.row];
    [cell setupWithCity:city];
    
    return cell;    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    City *city = self.cities[indexPath.row];
    [self.output didSelectCity:city];
}

#pragma mark - IBActions

- (IBAction)addCityTapped:(id)sender {
    [self.output didAskToAddCity];
}
@end