//
//  CityTableViewCell.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class City;

@interface CityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cityTitleLabel;

- (void)setupWithCity:(City *)city;

@end