//
//  CityTableViewCell.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 17/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "CityTableViewCell.h"

#import "City.h"

@implementation CityTableViewCell

- (void)setupWithCity:(City *)city {
    NSString *title = [NSString stringWithFormat:@"%@, %@", city.name, city.country];
    self.cityTitleLabel.text = title;
}

@end
