//
//  CitiesListViewController.h
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CitiesListViewInput.h"

@protocol CitiesListViewOutput;

@interface CitiesListViewController : UIViewController <CitiesListViewInput>

@property (nonatomic, strong) id<CitiesListViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *noCitiesFoundView;

- (IBAction)addCityTapped:(id)sender;

@end