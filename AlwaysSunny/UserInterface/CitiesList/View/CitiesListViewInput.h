//
//  CitiesListViewInput.h
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CitiesListViewInput <NSObject>

- (void)showCities:(NSArray *)cities;

@end