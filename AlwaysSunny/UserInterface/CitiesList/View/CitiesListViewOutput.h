//
//  CitiesListViewOutput.h
//  AlwaysSunny
//
//  Module: CitiesList
//  Description: CitiesList module
//
//  Created by Sergey Krapivenskiy on 17/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@class City;

@protocol CitiesListViewOutput <NSObject>

/**
 @author Sergey Krapivenskiy, 16-03-17 23:03:25
 
 Informs presenter that view is ready 
 */
- (void)didLoadView;

- (void)didDeleteCity:(City *)city;

- (void)didSelectCity:(City *)city;

- (void)didAskToAddCity;

@end