//
//  ForecastViewModelFactoryBase.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ForecastViewModelFactory.h"

@interface ForecastViewModelFactoryBase : NSObject <ForecastViewModelFactory>

@end
