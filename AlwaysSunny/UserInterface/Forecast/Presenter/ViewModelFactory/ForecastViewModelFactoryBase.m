//
//  ForecastViewModelFactoryBase.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "ForecastViewModelFactoryBase.h"

#import "ForecastViewModel.h"
#import "DailyForecast.h"


@interface ForecastViewModelFactoryBase ()

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation ForecastViewModelFactoryBase

- (NSArray *)viewModelsFromForecasts:(NSArray *)forecasts {
    NSMutableArray *result = [NSMutableArray new];
    for (DailyForecast *forecast in forecasts) {
        ForecastViewModel *viewModel = [self viewModelFromDailyForecast:forecast];
        if (viewModel) {
            [result addObject:viewModel];
        }
    }
    
    return [result copy];
}

#pragma mark - Private

- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd";
    }
    
    return _dateFormatter;
}

- (ForecastViewModel *)viewModelFromDailyForecast:(DailyForecast *)forecast {
    if ([forecast isKindOfClass:[DailyForecast class]] == NO ) {
        return nil;
    }
    
    ForecastViewModel *viewModel = [ForecastViewModel new];
    viewModel.dateString = forecast.dateString;
    viewModel.minTemperature = [NSString stringWithFormat:@"Min: %ld ºC ", (long)forecast.minTemperatureCelsius];
    viewModel.maxTemperature = [NSString stringWithFormat:@"Max: %ld ºC", (long)forecast.maxTemperatureCelsius];
    viewModel.sunrise = [NSString stringWithFormat:@"Sunrise: %@", forecast.astronomy.sunriseTime];
    viewModel.sunset = [NSString stringWithFormat:@"Sunset: %@", forecast.astronomy.sunsetTime];
    
    return viewModel;
}

@end