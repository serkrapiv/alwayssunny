//
//  ForecastViewModelFactory.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @author Sergey Krapivenskiy, 16-03-18 12:03:25
 
 Transforms forecast data models to view models that can be passed to view
 */
@protocol ForecastViewModelFactory <NSObject>

- (NSArray *)viewModelsFromForecasts:(NSArray *)forecasts;

@end
