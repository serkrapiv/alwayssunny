//
//  ForecastPresenter.h
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "ForecastViewOutput.h"
#import "ForecastInteractorOutput.h"
#import "ForecastModuleInput.h"

@protocol ForecastViewInput;
@protocol ForecastInteractorInput;
@protocol ForecastRouterInput;

@protocol ForecastViewModelFactory;

@interface ForecastPresenter : NSObject <ForecastModuleInput, ForecastViewOutput, ForecastInteractorOutput>

@property (nonatomic, weak) id<ForecastViewInput> view;
@property (nonatomic, strong) id<ForecastInteractorInput> interactor;
@property (nonatomic, strong) id<ForecastRouterInput> router;

@property (nonatomic, strong) id<ForecastViewModelFactory> viewModelFactory;

@end