//
//  ForecastModuleInput.h
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol ForecastModuleInput <RamblerViperModuleInput>

- (void)configureModuleWithCityName:(NSString *)cityName;

@end