//
//  ForecastPresenter.m
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "ForecastPresenter.h"

#import "ForecastViewInput.h"
#import "ForecastInteractorInput.h"
#import "ForecastRouterInput.h"

#import "ForecastViewModelFactory.h"

static const NSUInteger ALSDefaultNumberOfDays = 5;

@interface ForecastPresenter ()

@property (nonatomic, copy) NSString *cityName;

@end

@implementation ForecastPresenter

#pragma mark - ForecastModuleInput

- (void)configureModuleWithCityName:(NSString *)cityName {
    self.cityName = cityName;
}

#pragma mark - ForecastViewOutput

- (void)didLoadView {
    [self.view setupWithTitle:self.cityName];
    [self.interactor obtainForecastsForCityWithName:self.cityName
                                       numberOfDays:ALSDefaultNumberOfDays];
}

#pragma mark - ForecastInteractorOutput

- (void)didObtainForecasts:(NSArray *)forecasts {
    NSArray *viewModels = [self.viewModelFactory viewModelsFromForecasts:forecasts];
    [self.view showForecasts:viewModels];
}

- (void)didFailToObtainForecastsWithError:(NSError *)error {
    [self.view showErrorMessage:error.localizedDescription];
}

@end