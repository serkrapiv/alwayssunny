//
//  ForecastRouter.h
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "ForecastRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface ForecastRouter : NSObject <ForecastRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end