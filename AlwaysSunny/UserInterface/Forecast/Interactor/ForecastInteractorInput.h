//
//  ForecastInteractorInput.h
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ForecastInteractorInput <NSObject>

- (void)obtainForecastsForCityWithName:(NSString *)cityName
                          numberOfDays:(NSUInteger)numberOfDays;

@end