//
//  ForecastInteractorOutput.h
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ForecastInteractorOutput <NSObject>

- (void)didObtainForecasts:(NSArray *)forecasts;

- (void)didFailToObtainForecastsWithError:(NSError *)error;

@end