//
//  ForecastInteractor.h
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "ForecastInteractorInput.h"

@protocol ForecastInteractorOutput;
@protocol ForecastService;

@interface ForecastInteractor : NSObject <ForecastInteractorInput>

@property (nonatomic, weak) id<ForecastInteractorOutput> output;

@property (nonatomic, strong) id<ForecastService> forecastService;

@end