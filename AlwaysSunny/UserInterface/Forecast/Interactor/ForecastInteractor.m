//
//  ForecastInteractor.m
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "ForecastInteractor.h"

#import "ForecastInteractorOutput.h"

#import "ForecastService.h"

@implementation ForecastInteractor

#pragma mark - ForecastInteractorInput

- (void)obtainForecastsForCityWithName:(NSString *)cityName
                          numberOfDays:(NSUInteger)numberOfDays {
    __weak typeof(self) wSelf = self;
    [self.forecastService loadForecastsForCityWithName:cityName
                                          numberOfDays:numberOfDays
                                            completion:^(NSArray *objects, NSError *error)
    {
        if (error) {
            [wSelf.output didFailToObtainForecastsWithError:error];
        } else {
            [wSelf.output didObtainForecasts:objects];
        }
    }];
}

@end