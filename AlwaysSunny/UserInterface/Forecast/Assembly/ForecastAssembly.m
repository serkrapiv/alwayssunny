//
//  ForecastAssembly.m
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "ForecastAssembly.h"

#import "ForecastViewController.h"
#import "ForecastInteractor.h"
#import "ForecastPresenter.h"
#import "ForecastRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "ServicesAssembly.h"

#import "ForecastViewModelFactoryBase.h"

@interface ForecastAssembly ()

@property (nonatomic, strong) ServicesAssembly *servicesAssembly;

@end

@implementation ForecastAssembly

- (ForecastViewController *)viewForecastModule {
    return [TyphoonDefinition withClass:[ForecastViewController class]
                          configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterForecastModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterForecastModule]];
                          }];
}

- (ForecastInteractor *)interactorForecastModule {
    return [TyphoonDefinition withClass:[ForecastInteractor class]
                          configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterForecastModule]];
                              [definition injectProperty:@selector(forecastService) with:[self.servicesAssembly forecastService]];
                          }];
}

- (ForecastPresenter *)presenterForecastModule {
    return [TyphoonDefinition withClass:[ForecastPresenter class]
                          configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewForecastModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorForecastModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerForecastModule]];
                              [definition injectProperty:@selector(viewModelFactory)
                                                    with:[self viewModelFactoryForecastModule]];
                          }];
}

- (ForecastRouter *)routerForecastModule {

    return [TyphoonDefinition withClass:[ForecastRouter class]
                           configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewForecastModule]];
                          }];
}

- (id<ForecastViewModelFactory>)viewModelFactoryForecastModule {
    return [TyphoonDefinition withClass:[ForecastViewModelFactoryBase class]];
}

@end