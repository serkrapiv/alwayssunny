//
//  ForecastTableViewCell.m
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 18/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import "ForecastTableViewCell.h"

#import "ForecastViewModel.h"

@implementation ForecastTableViewCell

- (void)setupWithViewModel:(ForecastViewModel *)viewModel {
    self.dateLabel.text = viewModel.dateString;
    self.minTemperatureLabel.text = viewModel.minTemperature;
    self.maxTemperatureLabel.text = viewModel.maxTemperature;
    self.sunriseLabel.text = viewModel.sunrise;
    self.sunsetLabel.text = viewModel.sunset;
}

@end
