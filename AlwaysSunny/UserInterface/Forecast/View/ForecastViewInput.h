//
//  ForecastViewInput.h
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ForecastViewInput <NSObject>

- (void)setupWithTitle:(NSString *)title;

- (void)showForecasts:(NSArray *)forecasts;

- (void)showErrorMessage:(NSString *)errorMessage;

@end