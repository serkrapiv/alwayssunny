//
//  ForecastViewController.m
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "ForecastViewController.h"

#import "ForecastViewOutput.h"

#import "ForecastViewModel.h"
#import "ForecastTableViewCell.h"

@interface ForecastViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *forecasts;

@end

@implementation ForecastViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
	[super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    NSString *cellClassName = NSStringFromClass([ForecastTableViewCell class]);
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    UINib *nib = [UINib nibWithNibName:cellClassName bundle:bundle];
    [self.tableView registerNib:nib forCellReuseIdentifier:cellClassName];

	[self.output didLoadView];
}

#pragma mark - ForecastViewInput

- (void)setupWithTitle:(NSString *)title {
    self.navigationItem.title = title;
}

- (void)showForecasts:(NSArray *)forecasts {
    self.forecasts = [NSArray arrayWithArray:forecasts];
    [self.tableView reloadData];
}

- (void)showErrorMessage:(NSString *)errorMessage {
    NSLog(@"Forecast request failed with error %@", errorMessage);
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.forecasts.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = NSStringFromClass([ForecastTableViewCell class]);
    ForecastTableViewCell *cell = (ForecastTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    ForecastViewModel *viewModel = self.forecasts[indexPath.row];
    [cell setupWithViewModel:viewModel];
    return cell;
}

@end