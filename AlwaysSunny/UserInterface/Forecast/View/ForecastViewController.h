//
//  ForecastViewController.h
//  AlwaysSunny
//
//  Module: Forecast
//  Description: Forecast module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ForecastViewInput.h"

@protocol ForecastViewOutput;

@interface ForecastViewController : UIViewController <ForecastViewInput>

@property (nonatomic, strong) id<ForecastViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end