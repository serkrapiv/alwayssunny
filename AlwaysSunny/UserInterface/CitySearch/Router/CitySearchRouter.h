//
//  CitySearchRouter.h
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitySearchRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface CitySearchRouter : NSObject <CitySearchRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end