//
//  CitySearchRouter.m
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitySearchRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation CitySearchRouter

#pragma mark - CitySearchRouterInput

- (void)closeCurrentModule {
    [self.transitionHandler closeCurrentModule:YES];
}

@end