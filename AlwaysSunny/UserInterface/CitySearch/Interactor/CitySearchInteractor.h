//
//  CitySearchInteractor.h
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitySearchInteractorInput.h"

@protocol CitySearchInteractorOutput;
@protocol CitiesService;

@interface CitySearchInteractor : NSObject <CitySearchInteractorInput>

@property (nonatomic, weak) id<CitySearchInteractorOutput> output;

@property (nonatomic, strong) id<CitiesService> citiesService;

@end