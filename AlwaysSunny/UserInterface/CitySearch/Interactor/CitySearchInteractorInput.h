//
//  CitySearchInteractorInput.h
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@class City;

@protocol CitySearchInteractorInput <NSObject>

/**
 @author Sergey Krapivenskiy, 16-03-18 10:03:57
 
 Loads from server list of cities that match search query
 
 @param searchString Search query
 */
- (void)obtainCitiesForSearchString:(NSString *)searchString;

/**
 @author Sergey Krapivenskiy, 16-03-18 10:03:30
 
 Saves selected city in persistent storage
 */
- (void)addCity:(City *)city;

@end