//
//  CitySearchInteractor.m
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitySearchInteractor.h"

#import "CitySearchInteractorOutput.h"

#import "CitiesService.h"

@implementation CitySearchInteractor

#pragma mark - CitySearchInteractorInput

- (void)obtainCitiesForSearchString:(NSString *)searchString {
    if (searchString.length == 0) {
        [self.output didObtainCities:nil];
        return;
    }
    
    __weak typeof(self) wSelf = self;
    [self.citiesService loadCitiesForSearchString:searchString withCompletion:^(NSArray *objects, NSError *error)
     {
         if (error) {
             [wSelf.output didFailToObtainCitiesWithError:error];
         } else {
             [wSelf.output didObtainCities:objects];
         }
     }];
}

- (void)addCity:(City *)city {
    [self.citiesService addCity:city];
}

@end