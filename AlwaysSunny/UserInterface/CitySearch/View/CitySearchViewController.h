//
//  CitySearchViewController.h
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CitySearchViewInput.h"

@protocol CitySearchViewOutput;

@interface CitySearchViewController : UIViewController <CitySearchViewInput>

@property (nonatomic, strong) id<CitySearchViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *noResultsView;

@end