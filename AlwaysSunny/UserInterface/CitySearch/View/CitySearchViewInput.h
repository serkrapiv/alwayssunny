//
//  CitySearchViewInput.h
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CitySearchViewInput <NSObject>

/**
 @author Sergey Krapivenskiy, 16-03-18 10:03:44
 
 Shows list of cities
 
 @param cities List of cities
 */
- (void)showCities:(NSArray *)cities;

/**
 @author Sergey Krapivenskiy, 16-03-18 10:03:18
 
 Shows placeholder when there are no search results
 */
- (void)showNoResultsPlaceholder;

@end