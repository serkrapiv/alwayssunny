//
//  CitySearchViewController.m
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitySearchViewController.h"

#import "CitySearchViewOutput.h"

#import "CityTableViewCell.h"
#import "City.h"

@interface CitySearchViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchResultsUpdating>

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *cities;

@end

@implementation CitySearchViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
	[super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    NSString *cellClassName = NSStringFromClass([CityTableViewCell class]);
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    UINib *nib = [UINib nibWithNibName:cellClassName bundle:bundle];
    [self.tableView registerNib:nib forCellReuseIdentifier:cellClassName];
    
    [self setupSearchController];
    
    self.cities = [NSMutableArray new];
}

#pragma mark - Private

- (void)setupSearchController {
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    self.definesPresentationContext = YES;
}

#pragma mark - CitySearchViewInput

- (void)showCities:(NSArray *)cities {
    self.cities = [NSMutableArray arrayWithArray:cities];
    self.noResultsView.hidden = YES;
    [self.tableView reloadData];
}

- (void)showNoResultsPlaceholder {
    self.noResultsView.hidden = NO;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchString = searchController.searchBar.text;
    [self.output didAskToSearchForCityName:searchString];
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cities.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark - UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = NSStringFromClass([CityTableViewCell class]);
    CityTableViewCell *cell = (CityTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    City *city = self.cities[indexPath.row];
    [cell setupWithCity:city];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    City *city = self.cities[indexPath.row];
    [self.output didSelectCity:city];
    [self.searchController.searchBar resignFirstResponder];
    self.searchController.active = NO;
}

@end