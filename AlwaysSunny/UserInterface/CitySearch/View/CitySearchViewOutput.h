//
//  CitySearchViewOutput.h
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>

@class City;

@protocol CitySearchViewOutput <NSObject>

/**
 @author Sergey Krapivenskiy, 16-03-18 10:03:55
 
 Notifies presenter that user changed search query
 
 @param cityName Search query
 */
- (void)didAskToSearchForCityName:(NSString *)cityName;

/**
 @author Sergey Krapivenskiy, 16-03-18 10:03:15
 
 Notifies presenter that user did select city from search results
 
 @param city Selected city
 */
- (void)didSelectCity:(City *)city;

@end