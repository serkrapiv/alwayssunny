//
//  CitySearchAssembly.m
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitySearchAssembly.h"

#import "CitySearchViewController.h"
#import "CitySearchInteractor.h"
#import "CitySearchPresenter.h"
#import "CitySearchRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "ServicesAssembly.h"

@interface CitySearchAssembly ()

@property (nonatomic, strong) ServicesAssembly *servicesAssembly;

@end

@implementation CitySearchAssembly

- (CitySearchViewController *)viewCitySearchModule {
    return [TyphoonDefinition withClass:[CitySearchViewController class]
                          configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCitySearchModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterCitySearchModule]];
                          }];
}

- (CitySearchInteractor *)interactorCitySearchModule {
    return [TyphoonDefinition withClass:[CitySearchInteractor class]
                          configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCitySearchModule]];
                              [definition injectProperty:@selector(citiesService)
                                                    with:[self.servicesAssembly citiesService]];
                          }];
}

- (CitySearchPresenter *)presenterCitySearchModule {
    return [TyphoonDefinition withClass:[CitySearchPresenter class]
                          configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCitySearchModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCitySearchModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCitySearchModule]];
                          }];
}

- (CitySearchRouter *)routerCitySearchModule {

    return [TyphoonDefinition withClass:[CitySearchRouter class]
                           configuration:^(TyphoonDefinition *definition) 
                          {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCitySearchModule]];
                          }];
}

@end