//
//  CitySearchModuleInput.h
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol CitySearchModuleOutput;

@protocol CitySearchModuleInput <RamblerViperModuleInput>

- (void)configureModuleWithOutput:(id<CitySearchModuleOutput>)moduleOutput;

@end