//
//  CitySearchPresenter.m
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitySearchPresenter.h"

#import "CitySearchViewInput.h"
#import "CitySearchInteractorInput.h"
#import "CitySearchRouterInput.h"
#import "CitySearchModuleOutput.h"

@interface CitySearchPresenter ()

@property (nonatomic, weak) id<CitySearchModuleOutput> moduleOutput;

@end

@implementation CitySearchPresenter

#pragma mark - CitySearchModuleInput

- (void)configureModuleWithOutput:(id<CitySearchModuleOutput>)moduleOutput {
    self.moduleOutput = moduleOutput;
}

#pragma mark - CitySearchViewOutput

- (void)didAskToSearchForCityName:(NSString *)cityName {
    [self.interactor obtainCitiesForSearchString:cityName];
}

- (void)didSelectCity:(City *)city {
    [self.interactor addCity:city];
    [self.moduleOutput didSelectCityInSearch:city];
    [self.router closeCurrentModule];
}

#pragma mark - CitySearchInteractorOutput

- (void)didObtainCities:(NSArray *)cities {
    [self.view showCities:cities];
}

/**
 @author Sergey Krapivenskiy, 16-03-18 01:03:02
 
 For some reason weather API returns an error when there are no results that meet search query. So we might as well ignore these errors
 */
- (void)didFailToObtainCitiesWithError:(NSError *)error {
    [self.view showNoResultsPlaceholder];
}

@end