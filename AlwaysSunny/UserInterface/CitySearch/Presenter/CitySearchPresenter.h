//
//  CitySearchPresenter.h
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import "CitySearchViewOutput.h"
#import "CitySearchInteractorOutput.h"
#import "CitySearchModuleInput.h"

@protocol CitySearchViewInput;
@protocol CitySearchInteractorInput;
@protocol CitySearchRouterInput;

@interface CitySearchPresenter : NSObject <CitySearchModuleInput, CitySearchViewOutput, CitySearchInteractorOutput>

@property (nonatomic, weak) id<CitySearchViewInput> view;
@property (nonatomic, strong) id<CitySearchInteractorInput> interactor;
@property (nonatomic, strong) id<CitySearchRouterInput> router;

@end