//
//  CitySearchModuleOutput.h
//  AlwaysSunny
//
//  Module: CitySearch
//  Description: CitySearch module
//
//  Created by Sergey Krapivenskiy on 18/03/2016.
//  Copyright 2016 serkrapiv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@class City;

@protocol CitySearchModuleOutput <RamblerViperModuleOutput>

- (void)didSelectCityInSearch:(City *)city;

@end