//
//  AppDelegate.h
//  AlwaysSunny
//
//  Created by Sergey Krapivenskiy on 16/03/16.
//  Copyright © 2016 serkrapiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

